DELIMITER //

/*
 * WEBREMINDER2
 *
 * Last revison: 16.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 */
 

--
-- TABULKY
--

CREATE TABLE log
 ( id                       INT NOT NULL AUTO_INCREMENT, 
   date                     TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
   subject_id               INT COMMENT 'Id uživatele', 
   case_id                  INT COMMENT 'Id uživatele', 
   module                   VARCHAR(200) COMMENT 'Název modulu', 
   presenter                VARCHAR(200) COMMENT 'Název presenteru', 
   action                   VARCHAR(200) COMMENT 'Název akce',
   status                   INT NOT NULL DEFAULT 1 COMMENT 'Stav',
   remote_addr              VARCHAR(200) COMMENT 'Adresa odkud se přistupuje', 
   ip_addr                  VARCHAR(20) COMMENT 'IP adresa odkud se přistupuje',
   browser                  VARCHAR(2000) COMMENT 'Údaje o prohlížeči',
   description              VARCHAR(2000) COMMENT 'Podrobnější popis',
   data                     TEXT COMMENT 'Podrobnější data',
PRIMARY KEY (id), 
KEY log_i1 (date, subject_id),
KEY log_i2 (subject_id, date),
KEY log_i3 (date, module, presenter, action),  
KEY log_i4 (module, presenter, action, date),
KEY log_i5 (subject_id, module, presenter, action, date) )
ENGINE=InnoDb MAX_ROWS=1000000000 COMMENT='Logovací tabulka'
//


CREATE TABLE language
 ( id                       INT NOT NULL COMMENT 'Id jayzka',
   code                     VARCHAR(20) NOT NULL COMMENT 'Kód jazyka (národního prostředí v PHP)',
   name                     VARCHAR(60) NOT NULL COMMENT 'Název jazyka',
   db_environment           VARCHAR(20) NOT NULL COMMENT 'Kód databázového národního prostředí',
   dbchanged                TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (id),
UNIQUE KEY language_i1 (code),
UNIQUE KEY language_i2 (name) )
ENGINE=InnoDb MAX_ROWS=10 COMMENT='Jazyky aplikace'
//


CREATE TABLE param
 ( param                    VARCHAR(60) NOT NULL COMMENT 'Kód parametru',
   level                    ENUM ('B', 'S') NOT NULL DEFAULT 'S' COMMENT 'Úroveň parametru(B=Základ, S=System)', 
   type                     ENUM ('C', 'N', 'D', 'B', 'L') NOT NULL DEFAULT 'C' COMMENT 'Typ parametru (C=char, N=Number, D=Date, B=Boolean, L=List)',
   default_value            VARCHAR(2000) COMMENT 'Standardní hodnota parametru',
   value                    VARCHAR(2000) COMMENT 'Aktuální hodnota parametru',
   description              VARCHAR(2000) COMMENT 'Popis parametru',
   created_at               DATETIME NOT NULL COMMENT 'Datum a čas založení záznamu', 
   created_by               INT NOT NULL COMMENT 'Aplikační uživatel, který záznam založil', 
   created_by_db            VARCHAR(64) NOT NULL COMMENT 'Databázový uživatel, který záznam založil',
   updated_at               DATETIME COMMENT 'Datum a čas poslední změny záznamu', 
   updated_by               INT COMMENT 'Aplikační uživatel, který záznam poslední změnil', 
   updated_by_db            VARCHAR(64) COMMENT 'Databázový uživatel, který záznam poslední změnil',
   dbchanged                TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
PRIMARY KEY (param) )
ENGINE=InnoDb MAX_ROWS=500 COMMENT='Parametry aplikace'
//

CREATE TRIGGER param_bi BEFORE INSERT ON param
   FOR EACH ROW
BEGIN
   CALL set_stamp(NEW.created_at, NEW.created_by, NEW.created_by_db);
END;
//
CREATE TRIGGER param_bu BEFORE UPDATE ON param
   FOR EACH ROW
BEGIN
   CALL set_stamp(NEW.updated_at, NEW.updated_by, NEW.updated_by_db);
END;
//


CREATE TABLE param_list
 ( id                       INT NOT NULL AUTO_INCREMENT COMMENT 'Id záznamu',
   param                    VARCHAR(60) NOT NULL COMMENT 'Kód parametru',
   sentence                 INT NOT NULL DEFAULT 0 COMMENT 'Pořadí hodnoty', 
   value                    VARCHAR(200) NOT NULL COMMENT 'Hodnota', 
   description              VARCHAR(200) NOT NULL COMMENT 'Popis hodnoty', 
   created_at               DATETIME NOT NULL COMMENT 'Datum a čas založení záznamu', 
   created_by               INT NOT NULL COMMENT 'Aplikační uživatel, který záznam založil', 
   created_by_db            VARCHAR(64) NOT NULL COMMENT 'Databázový uživatel, který záznam založil',
   updated_at               DATETIME COMMENT 'Datum a čas poslední změny záznamu', 
   updated_by               INT COMMENT 'Aplikační uživatel, který záznam poslední změnil', 
   updated_by_db            VARCHAR(64) COMMENT 'Databázový uživatel, který záznam poslední změnil',
   dbchanged                TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
PRIMARY KEY (id),
UNIQUE KEY param_list_i1 (param, sentence, value),
UNIQUE KEY param_list_i2 (param, value),
UNIQUE KEY param_list_i3 (param, description),
CONSTRAINT param_list_c1 FOREIGN KEY (param) REFERENCES param (param) ON UPDATE CASCADE ON DELETE CASCADE )
ENGINE=InnoDb MAX_ROWS=500 COMMENT='Parametry aplikace'
//

CREATE TRIGGER param_list_bi BEFORE INSERT ON param_list
   FOR EACH ROW
BEGIN
   CALL set_stamp(NEW.created_at, NEW.created_by, NEW.created_by_db);
END;
//
CREATE TRIGGER param_list_bu BEFORE UPDATE ON param_list
   FOR EACH ROW
BEGIN
   CALL set_stamp(NEW.updated_at, NEW.updated_by, NEW.updated_by_db);
END;
//


CREATE TABLE user
 ( id                       INT NOT NULL AUTO_INCREMENT COMMENT 'Id uživatele',
   login                    VARCHAR(60) NOT NULL COMMENT 'Login uživatele', 
   name                     VARCHAR(60) NOT NULL COMMENT 'Jméno uživatele',
   password                 VARCHAR(255) NOT NULL COMMENT 'Heslo uživatele', 
   email                    VARCHAR(255) NOT NULL COMMENT 'E-mail uživatele', 
   description              VARCHAR(2000) COMMENT 'Popis uživatele', 
   enabled                  BOOLEAN NOT NULL DEFAULT TRUE COMMENT 'Je uživatel aktivní?',
   password_at              DATETIME COMMENT 'Datum poslední změny hesla', 
   password_by              INT COMMENT 'Id uživatele, který poslední změnil heslo', 
   password_by_db           VARCHAR(60) COMMENT 'Název db uživatele, který poslední změnil heslo', 
   created_at               DATETIME NOT NULL COMMENT 'Datum a čas založení záznamu', 
   created_by               INT NOT NULL COMMENT 'Aplikační uživatel, který záznam založil', 
   created_by_db            VARCHAR(64) NOT NULL COMMENT 'Databázový uživatel, který záznam založil',
   updated_at               DATETIME COMMENT 'Datum a čas poslední změny záznamu', 
   updated_by               INT COMMENT 'Aplikační uživatel, který záznam poslední změnil', 
   updated_by_db            VARCHAR(64) COMMENT 'Databázový uživatel, který záznam poslední změnil',
   dbchanged                TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
PRIMARY KEY (id), 
UNIQUE KEY user_i1 (login), 
UNIQUE KEY user_i2 (email), 
UNIQUE KEY user_i3 (enabled, login) )
ENGINE=InnoDb MAX_ROWS=100 COMMENT='Uživatelé'
//

CREATE TRIGGER user_bi BEFORE INSERT ON user
   FOR EACH ROW
BEGIN
   CALL set_stamp(NEW.created_at, NEW.created_by, NEW.created_by_db);
   SET NEW.password_at = now();
   SET NEW.password_by = get_appl_user();
   SET NEW.password_by_db = current_user();
END;
//
CREATE TRIGGER user_bu BEFORE UPDATE ON user
   FOR EACH ROW
BEGIN
   CALL set_stamp(NEW.updated_at, NEW.updated_by, NEW.updated_by_db);
   IF NEW.password != OLD.password
   THEN SET NEW.password_at = now();
        SET NEW.password_by = get_appl_user();
        SET NEW.password_by_db = current_user();
   END IF;
END;
//


CREATE TABLE sentence
 ( sentence                 INT NOT NULL,
PRIMARY KEY (sentence ) )
ENGINE=InnoDb MAX_ROWS=100000 COMMENT='Tabulka počtů'
//


CREATE TABLE calendar
 ( date                     DATE NOT NULL, 
   holiday                  BOOLEAN NOT NULL DEFAULT FALSE, 
   description              VARCHAR(2000),
   created_at               DATETIME NOT NULL COMMENT 'Datum a čas založení záznamu', 
   created_by               INT NOT NULL COMMENT 'Aplikační uživatel, který záznam založil', 
   created_by_db            VARCHAR(64) NOT NULL COMMENT 'Databázový uživatel, který záznam založil',
   updated_at               DATETIME COMMENT 'Datum a čas poslední změny záznamu', 
   updated_by               INT COMMENT 'Aplikační uživatel, který záznam poslední změnil', 
   updated_by_db            VARCHAR(64) COMMENT 'Databázový uživatel, který záznam poslední změnil',
   dbchanged                TIMESTAMP ON UPDATE CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP, 
PRIMARY KEY (date), 
UNIQUE KEY calendar_i1 (holiday, date))
ENGINE=InnoDb MAX_ROWS=500000 COMMENT='Tabulka kalendáře'
//

CREATE TRIGGER calendar_bi BEFORE INSERT ON calendar
   FOR EACH ROW
BEGIN
   CALL set_stamp(NEW.created_at, NEW.created_by, NEW.created_by_db);
END;
//
CREATE TRIGGER calendar_bu BEFORE UPDATE ON calendar
   FOR EACH ROW
BEGIN
   CALL set_stamp(NEW.updated_at, NEW.updated_by, NEW.updated_by_db);
END;
//


--
-- PROCEDURY A FUNKCE
--


CREATE PROCEDURE set_appl_user ( i_user INT )
   COMMENT 'Nastaví id aktuálního aplikačního uživatele'
   SQL SECURITY INVOKER 
BEGIN
   SET @appl_user := i_user;
END;
//


CREATE FUNCTION get_appl_user () RETURNS INT
   NO SQL
   COMMENT 'Vrací id aktuálního aplikačního uživatele'
   SQL SECURITY INVOKER 
BEGIN
   RETURN IFNULL(@appl_user, 0);
END;
//


CREATE PROCEDURE set_stamp ( INOUT o_date DATETIME, INOUT o_user_id INT, INOUT o_user_db VARCHAR(64) )
   COMMENT 'Pro logování přístupu k záznamu'
   SQL SECURITY INVOKER 
BEGIN
   SET o_date = now();
   SET o_user_id = get_appl_user();
   SET o_user_db = current_user();
END;
//


CREATE PROCEDURE set_sentence
 ( p_from INT,
   p_to INT )
   COMMENT 'Nastavení počítadla' 
   SQL SECURITY INVOKER
BEGIN 
DECLARE a_count INT;
  SET a_count = p_from;
  WHILE a_count <= p_to DO
     INSERT INTO sentence (sentence) VALUES (a_count);
     SET a_count = a_count + 1;
  END WHILE;
END;
//


CREATE PROCEDURE set_calendar
 ( p_date_from DATE,
   p_date_to DATE )
   COMMENT 'Nastavení kalendáře' 
   SQL SECURITY INVOKER
BEGIN 
DECLARE a_date DATE;
DECLARE a_holiday BOOLEAN;
  SET a_date = p_date_from;
  WHILE a_date <= p_date_to DO
     IF DATE_FORMAT(a_date, '%w') = 0 OR DATE_FORMAT(a_date, '%w') = 6
     THEN SET a_holiday = TRUE;
     ELSE SET a_holiday = FALSE;
     END IF;
     INSERT INTO calendar (date, holiday) VALUES (a_date, a_holiday);
     SET a_date = DATE_ADD(a_date, INTERVAL 1 DAY);
  END WHILE;
END;
//


CREATE FUNCTION get_system_param
  ( p_param VARCHAR(60) ) RETURNS VARCHAR(2000)
   COMMENT 'Vrací hodnotu zadaného parametru'
   READS SQL DATA
   SQL SECURITY INVOKER
BEGIN
  DECLARE x_value VARCHAR(2000);
  SELECT value
  INTO   x_value
  FROM   param
  WHERE  param = p_param;
  RETURN x_value;
END;
//


CREATE FUNCTION date_output
  ( p_date DATETIME ) RETURNS VARCHAR(60)
   COMMENT 'Vrací formátované datum'
   READS SQL DATA
   SQL SECURITY INVOKER
BEGIN
  DECLARE x_format VARCHAR(200);
  SET x_format = IFNULL(get_system_param('dateFormatOutput'), '%e.%c.%Y');
  RETURN DATE_FORMAT(p_date, x_format);
END;
//


CREATE FUNCTION datetime_output
  ( p_date DATETIME ) RETURNS VARCHAR(60)
   COMMENT 'Vrací formátované datum a čas'
   READS SQL DATA
   SQL SECURITY INVOKER
BEGIN
  DECLARE x_format VARCHAR(200);
  SET x_format = IFNULL(get_system_param('datetimeFormatOutput'), '%e.%c.%Y %H:%i:%s');
  RETURN DATE_FORMAT(p_date, x_format);
END;
//


CREATE FUNCTION time_output
  ( p_date DATETIME ) RETURNS VARCHAR(60)
   COMMENT 'Vrací formátovaný čas'
   READS SQL DATA
   SQL SECURITY INVOKER
BEGIN
  DECLARE x_format VARCHAR(200);
  SET x_format = IFNULL(get_system_param('timeFormatOutput'), '%H:%i:%s');
  RETURN DATE_FORMAT(p_date, x_format);
END;
//



--
-- POHLEDY
--

CREATE VIEW user_v AS
SELECT U.id id,
       U.login login, 
       U.name name, 
       U.password password, 
       U.email email, 
       U.description description,
       U.enabled, 
       U.password_at password_at, 
       datetime_output(U.password_at) password_at_f, 
       U.password_by password_by, 
       U3.name password_by_name, 
       U.password_by_db password_by_db, 
       U.created_at created_at, 
       datetime_output(U.created_at) created_at_f, 
       U.created_by created_by, 
       U1.name created_by_name, 
       U.created_by_db created_by_db, 
       U.updated_at updated_at, 
       datetime_output(U.updated_at) updated_at_f, 
       U.updated_by updated_by, 
       U2.name updated_by_name, 
       U.updated_by_db updated_by_db, 
       CASE WHEN U.updated_at IS NULL THEN U.created_at ELSE U.updated_at END changed_at, 
       CASE WHEN U.updated_at IS NULL THEN datetime_output(U.created_at) ELSE datetime_output(U.updated_at) END changed_at_f, 
       CASE WHEN U.updated_at IS NULL THEN U.created_by ELSE U.updated_by END changed_by, 
       CASE WHEN U.updated_at IS NULL THEN U1.name ELSE U2.name END changed_by_name, 
       CASE WHEN U.updated_at IS NULL THEN U.created_by_db ELSE U.updated_by_db END changed_by_db, 
       U.dbchanged dbchanged
FROM user U
LEFT JOIN user U1 ON U1.id = U.created_by 
LEFT JOIN user U2 ON U2.id = U.updated_by 
LEFT JOIN user U3 ON U3.id = U.password_by
//


CREATE VIEW user_login_v AS
SELECT U.id id, 
       U.login login,
       U.name name, 
       U.password password, 
       U.email email,
       U.description description,
       now() actual_datetime, 
       datetime_output(now()) actual_datetime_f
FROM user U
WHERE U.enabled = TRUE
//


CREATE VIEW param_v AS
SELECT P.param param, 
       P.level level, 
       P.type type, 
       P.value value, 
       IFNULL(L.description, P.value) value_show, 
       P.description description,
       P.created_at created_at, 
       datetime_output(P.created_at) created_at_f, 
       P.created_by created_by, 
       U1.name created_by_name, 
       P.created_by_db, 
       P.updated_at updated_at, 
       datetime_output(P.updated_at) updated_at_f, 
       P.updated_by updated_by, 
       U2.name updated_by_name, 
       P.updated_by_db updated_by_db, 
       CASE WHEN P.updated_at IS NULL THEN P.created_at ELSE P.updated_at END changed_at,
       CASE WHEN P.updated_at IS NULL THEN datetime_output(P.created_at) ELSE datetime_output(P.updated_at) END changed_at_f,
       CASE WHEN P.updated_at IS NULL THEN P.created_by ELSE P.updated_by END changed_by,
       CASE WHEN P.updated_at IS NULL THEN U1.name ELSE U2.name END changed_by_name,
       CASE WHEN P.updated_at IS NULL THEN P.created_by_db ELSE P.updated_by_db END changed_by_db,
       P.dbchanged
FROM param P
LEFT JOIN param_list L ON P.param = P.param AND P.value = P.value AND P.type = 'L'
LEFT JOIN user U1 ON U1.id = P.created_by
LEFT JOIN user U2 ON U2.id = P.updated_by
//



/*
CREATE VIEW subject_v AS
SELECT S.id id, 
       S.code code, 
       S.name name
FROM dual S
//
*/


CREATE VIEW subject_v AS
SELECT 1 id, 
       '12815' code, 
       'Novák Josef' name
FROM dual
//



CREATE VIEW log_v AS
SELECT L.id id, 
       L.date date, 
       datetime_output(L.date) date_f, 
       date_output(L.date) date_df, 
       L.subject_id subject_id, 
       S.code subject_code,
       S.name subject_name,
       L.case_id case_id, 
       L.module module, 
       L.presenter presenter, 
       L.action action, 
       L.remote_addr remote_addr, 
       L.ip_addr ip_addr, 
       L.browser browser, 
       L.status status, 
       L.description description,
       L.data data 
FROM log L
LEFT JOIN subject_v S ON S.id = L.subject_id
//


CREATE VIEW log_login_v AS
SELECT L.id id, 
       L.date date, 
       datetime_output(L.date) date_f, 
       date_output(L.date) date_df, 
       L.subject_id subject_id, 
       S.code subject_code,
       S.name subject_name
FROM log L
LEFT JOIN subject_v S ON S.id = L.subject_id
WHERE L.module = 'Sign'
AND   L.presenter = 'Login'
AND   L.action = 'step4'
AND   L.status = 1
//


CREATE VIEW log_sms_v AS
SELECT L.id id, 
       L.date date, 
       datetime_output(L.date) date_f, 
       date_output(L.date) date_df, 
       L.subject_id subject_id, 
       S.code subject_code,
       S.name subject_name,
       L.description phone
FROM log L
LEFT JOIN subject_v S ON S.id = L.subject_id
WHERE L.module = 'Sign'
AND   L.presenter = 'Login'
AND   L.action = 'step3'
AND   L.status = 1
//


--
-- ZÁKLADNÍ DATA
--

START TRANSACTION
//


INSERT INTO language
 (id, code, name, db_environment)
VALUES  (1, 'cz', 'Češtins', 'cz'),
        (2, 'en', 'English', 'en')
//


-- Parametry formátu datumu
--
INSERT INTO param (type, level, param, description, value)
VALUES ('L', 'S', 'dateFormatOutput', 'Formát výstupního datumu', '%e.%c.%Y')
//
INSERT INTO param_list (param, sentence, value, description)
VALUES ('dateFormatOutput', 1, '%e.%c.%Y', 'dd.mm.rrrr' ),
       ('dateFormatOutput', 2, '%e-%c-%Y', 'dd-mm-rrrr' ),
       ('dateFormatOutput', 3, '%Y-%c-%e', 'rrrr-dd-mm' )
//
       
INSERT INTO param (type, level, param, description, value)
VALUES ('L', 'S', 'datetimeFormatOutput', 'Formát výstupního datumu a času', '%e.%c.%Y %H:%i:%s')
//
INSERT INTO param_list (param, sentence, value, description)
VALUES ('datetimeFormatOutput', 1, '%e.%c.%Y %H:%i:%s', 'dd.mm.rrrr hh:mi:ss'),
       ('datetimeFormatOutput', 2, '%e-%c-%Y %H:%i:%s', 'dd-mm-rrrr hh:mi:ss'),
       ('datetimeFormatOutput', 3, '%Y-%c-%e %H:%i:%s', 'rrrr-dd-mm hh:mi:ss'),
       ('datetimeFormatOutput', 4, '%e.%c.%Y %H:%i', 'dd.mm.rrrr hh:mi'),
       ('datetimeFormatOutput', 5, '%e-%c-%Y %H:%i', 'dd-mm-rrrr hh:mi'),
       ('datetimeFormatOutput', 6, '%Y-%c-%e %H:%i', 'rrrr-dd-mm hh:mi')
//

INSERT INTO param (type, level, param, description, value)
VALUES ('L', 'S', 'timeFormatOutput', 'Formát výstupního času', '%H:%i:%s')
//
INSERT INTO param_list (param, sentence, value, description)
VALUES ('timeFormatOutput', 1, '%H:%i:%s', 'hh:mi:ss'),
       ('timeFormatOutput', 2, '%H:%i', 'hh:mi')
//


INSERT INTO param (type, level, param, description, value)
VALUES ('N', 'S', 'maxPictureSize', 'Maximální velikost obrázku', '2084882')
//

INSERT INTO param (type, level, param, description, value)
VALUES ('N', 'S', 'maxDocumentSize', 'Maximální velikost dokumentu', '2084882')
//

INSERT INTO param (type, level, param, description, value)
VALUES ('N', 'S', 'minUserPasswordLength', 'Minimální délka hesla uživatele', '5')
//


INSERT INTO param (type, level, param, description, value)
VALUES ('N', 'S', 'pageCount1', 'Počet záznamů při výpisu na úrovni 1', '15');
//

INSERT INTO param (type, level, param, description, value)
VALUES ('N', 'S', 'pageCount2', 'Počet záznamů při výpisu na úrovni 2', '10');
//

INSERT INTO param (type, level, param, description, value)
VALUES ('N', 'S', 'userExpiration', 'Odhlášení uživatele při nečinnosti (v minutách)', '30');
//


--
--    Naplnění tabulky sentence
--
CALL set_sentence(1, 9999)
//


--
--    Naplnění tabulky calendar
--
CALL set_calendar('1990-01-01', '2030-12-31')
//


INSERT INTO user (login, email, name, password, description, enabled)
VALUES ('admin', 'admin@admin.cz', 'Administrátor', SHA1('admin1'), NULL, TRUE);


COMMIT
//

