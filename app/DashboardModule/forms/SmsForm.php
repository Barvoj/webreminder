<?php
/**
 * WebReminder - AKLH
 *
 * Last revison: 4.5.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 *
 * Třída pro vytvoření formulářů pro práci se SMS
 */


namespace DashboardModule;

use \Nette\Application\UI\Form;


class SmsForm extends \Nette\Object
{

   /** @var type Presenter */
   protected $presenter;

   
   /**
    * Vytvoření instance
    * @param presenter $presenter Presenter
    */
   public function __construct($presenter)
   {
      $this->presenter = $presenter;
   }
    
    
   /**
    * Vytvoření formuláře pro zaslání platebních symbolů pomocí SMS
    * @param int $subjectId Id subjektu
    * @param string $action Akce po submitu formuláře
    * @return Form Instance formuláře
    */
   public function paymentSymbol($subjectId, $action, $actionBack)
   {
      $form = new Form($this->presenter, 'payment');
      $form->setTranslator($this->presenter->translator);
      $form->addSelect('medium', '', array('email'=>'E-MAIL', 'sms'=>'SMS'));
      
      $phoneOptions = $this->presenter->loginModel->selectPhone($subjectId);
      $form->addSelect('phone', '', $phoneOptions)->setPrompt(' - Vyberte - ');

      $emailOptions = $this->presenter->loginModel->selectEmail($subjectId);
      $form->addSelect('email', '', $emailOptions)->setPrompt(' - Vyberte - ');
      
      $form->addText('phone_new', '', NULL, 255);
      $form->addText('email_new', '', NULL, 255);
      $form->addSubmit('back', 'Zpět bez akce')->setValidationScope(FALSE)->onClick[] = array($this->presenter, $actionBack);
      $form->addSubmit('send', 'Odeslat SMS')->onClick[] = array($this->presenter, $action);
      return $form;
   }

   
   /**
    * Vytvoření formuláře pro zaslání zprávy Zavolej mi zpět
    * @param int $subjectId Id subjektu
    * @param string $action Akce po submitu formuláře
    * @return Form Instance formuláře
    */
   public function callMe($subjectId, $action, $actionBack)
   {
      $form = new Form($this->presenter, 'payment');
      $form->setTranslator($this->presenter->translator);
      $form->addSelect('phone', '', $this->presenter->loginModel->selectPhone($subjectId))->setPrompt(' - Vyberte - ');
      $form->addText('phone_new', '', NULL, 255);
      $form->addText('date', '', NULL, 255)->setAttribute("autocomplete", "off");
      
      $zones = $this->generateTimeZones();
      $form->addSelect('time', '', $zones)->setPrompt(' - Vyberte - ');
      $form->addTextArea('description', '', 200, 4);
      $form->addSubmit('back', 'Zpět bez akce')->setValidationScope(FALSE)->onClick[] = array($this->presenter, $actionBack);
      $form->addSubmit('send', 'Odeslat')->onClick[] = array($this->presenter, $action);
      return $form;
   }
   
   /**
    * Vytvoření formuláře pro zaslání zprávy Osobní návštěva
    * @param int $subjectId Id subjektu
    * @param string $action Akce po submitu formuláře
    * @return Form Instance formuláře
    */
   public function personalVisit($subjectId, $action, $actionBack)
   {
      $form = new Form($this->presenter, 'payment');
      $form->setTranslator($this->presenter->translator);
      $form->addSelect('address', '', $this->presenter->loginModel->selectAddress($subjectId))->setPrompt(' - Vyberte - ');
      $form->addText('address_new', '', NULL, 255);
      $form->addText('date', '', NULL, 255)->setAttribute("autocomplete", "off");
      
      $zones = $this->generateTimeZones();
      $form->addSelect('time', '', $zones)->setPrompt(' - Vyberte - ');
      $form->addTextArea('description', '', 200, 4);
      $form->addSubmit('back', 'Zpět bez akce')->setValidationScope(FALSE)->onClick[] = array($this->presenter, $actionBack);
      $form->addSubmit('send', 'Odeslat')->onClick[] = array($this->presenter, $action);
      return $form;
   }
   
   /**
    * Vygeneruje pole s časovými pásmi
    * @return string
    */
   protected function generateTimeZones()
   {
       $zones = array();
       
       $from = $this->presenter->paramConfig['contactCenter']['from'];
       $to = $this->presenter->paramConfig['contactCenter']['to'];
       $range = $this->presenter->paramConfig['contactCenter']['range'];
       
       for($i = $from; $i < $to; $i+=$range)
       {
           $zones[$i] = sprintf("%02d", $i).":00 - ".sprintf("%02d", min(($i+$range), $to)).":00";
       }
       
       return $zones;
   }

   /**
    * Vytvoření formuláře pro zaslání zprávy Vyjádření k případu
    * @param int $subjectId Id subjektu
    * @param string $action Akce po submitu formuláře
    * @return Form Instance formuláře
    */
   public function statement($subjectId, $action, $actionBack)
   {
      $form = new Form($this->presenter, 'payment');
      $form->setTranslator($this->presenter->translator);
      $form->addSelect('type', '', $this->presenter->fileModel->selectFileTypes())->setPrompt(' - Vyberte - ');
      $form->addUpload('file');
      $form->addTextArea('description', '', 200, 4);
      $form->addSubmit('back', 'Zpět bez akce')->setValidationScope(FALSE)->onClick[] = array($this->presenter, $actionBack);
      $form->addSubmit('send', 'Odeslat')->onClick[] = array($this->presenter, $action);
      return $form;
   }
   
   /**
    * Vytvoření formuláře pro zaslání zprávy Splátkový kalendář
    * @param string $action Akce po submitu formuláře
    * @return Form Instance formuláře
    */
   public function paymentPlan($action, $actionBack, $minPaymentCount = 0, $maxPaymentCount = 10, $minPaymentValue = 0)
   {
      $form = new Form($this->presenter, 'payment');
      $form->setTranslator($this->presenter->translator);
      
      $form->addHidden('calculation_type');
      $form->addText('total_payments', '', NULL, 255)
              ->setRequired("Není vyplněn celkový počet splátek")
              ->addRule(Form::INTEGER)
              ->addRule(Form::RANGE, 'Počet splátek musí být %d do %d', array($minPaymentCount, $maxPaymentCount))
              ->setType('number');
      $form->addText('payment_value', '', NULL, 255)
              ->setRequired("Není vyplněna měsíční splátka")
              ->addRule(Form::FLOAT)
              ->addRule(Form::MIN, "Minimální výše splátky je %d", $minPaymentValue);
      $form->addText('first_payment_date', '', NULL, 255)
              ->setAttribute("autocomplete", "off")
              ->setRequired('Není vyplněno datum první splátky');
      $form->addText('maturity_day', '', null, 255)
              ->setRequired("Není vyplněn den splatnosti")
              ->addRule(Form::INTEGER)
              ->addRule(Form::RANGE, 'Den musí být %d do %d', array(1, 31))
              ->setType('number');
      
      $form->addSubmit('back', 'Zpět bez akce')->setValidationScope(FALSE)->onClick[] = array($this->presenter, $actionBack);
      $form->addSubmit('send', 'Odeslat')->onClick[] = array($this->presenter, $action);
      return $form;
   }
}
