<?php
/**
 * WebReminder - AKLH
 *
 * Last revison: 20.6.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Presenter pro práci se SMS
 */


namespace DashboardModule;

use \Nette\Mail\Message;



class VisitPresenter extends \BaseModule\LoginPresenter
{
    const DATE_ACTION_CALL = 'callMeBack', 
        DATE_ACTION_VISIT = 'personalVisit';

   public $loginModel;
   public $smsForm;
   protected $subject;
   protected $actionLogModel;
   protected $contactServiceModel;
   protected $taskModel;
    
   
   public function startup()
   {
      parent::startup();
      $this->loginModel = new \SignModule\LoginModel($this->db2, $this->translator);
      $this->actionLogModel = new LogModel($this->db1);
      $this->contactServiceModel = new \DashboardModule\ContactServiceModel($this->db2);
      $this->taskModel = new \DashboardModule\TaskModel($this->db2);
      
      $this->smsForm = new SmsForm($this);
      $this->subject = $this->getUser()->getIdentity()->subject_id;
   }
   
   /**
    * Akce - Zavolej mi zpět
    */
   public function actionCallMe()
   {
      $this->template->form = $this->smsForm->callMe($this->subject, 'submitCallMe', 'submitCallMeBack');
      
      if( isset($this->paramConfig['priceList']['callMe']) 
               && $this->paramConfig['priceList']['callMe'][0] > 0 )
       {
           $this->template->price = $this->paramConfig['priceList']['callMe'];
       }
   }

   /**
    * Akce - Zavolej mi zpět - Vlastní realizace akce
    * @param Nette\Forms\Controls\SubmitButton $button Submit button formuláře
    */
   public function submitCallMe(\Nette\Forms\Controls\SubmitButton $button)
   {
      $data = $button->getForm()->getValues();
      $phone = $data['phone_new'] == NULL ? $data['phone'] : $data['phone_new'];
      if ($phone == NULL)
      {
          $message = $this->translator->translate('Není zadáno nebo vybráno žádné telefonní spojení');                            // Přidání chyby do formuláře
          $button->getForm()->addError($message);
          return FALSE;
      }
      $phoneValid = \BaseModule\Validator::phoneNumber($phone);
      if ($phoneValid === FALSE)
      {
         $button->getForm()->addError($this->translator->translate('Chybný tvar telefonního čísla'));                             // Přidání chyby do formuláře
         return FALSE;
      }
      if ($data['date'] === "")
      {
          $message = $this->translator->translate('Není zadáno datum hovoru');                                           // Přidání chyby do formuláře
          $button->getForm()->addError($message);
          return FALSE;
      }
      $date = \DateTime::createFromFormat ( "d.m.Y" , $data['date']);
      if ($date === false)
      {
          $message = $this->translator->translate('Chybný tvar data hovoru');                                           // Přidání chyby do formuláře
          $button->getForm()->addError($message);
          return FALSE;
      }
      if(!$this->isDatePermissible($date, self::DATE_ACTION_CALL))
      {
          $message = $this->translator->translate('Datum hovoru musí být pracovní den');                                           // Přidání chyby do formuláře
          $button->getForm()->addError($message);
          return FALSE;
      }
      if ($data['time'] === null)
      {
          $message = $this->translator->translate('Není vybrán žádný čas hovoru');                                           // Přidání chyby do formuláře
          $button->getForm()->addError($message);
          return FALSE;
      }
      
      /* @var $date \DateTime */
      $date->setTime($data['time'], 0, 0);
      $description = $this->translator->translate('Vyžádaný hovor zpět na').' '.$phoneValid;
      $this->taskModel->addUserTask(14, $date, $description);
       
      if ($data['phone_new'] != NULL)                                                                                             // Zápis nového telefonního čísla (pokud bylo zadáno)
      {
         $this->contactServiceModel->begin();
         $this->contactServiceModel->insertPhone($this->subject, $data['phone_new']);                                             // Zápis kontaktu do DB
         $this->contactServiceModel->commit();
      }
      $this->actionLogModel->begin();
      $this->actionLogModel->insert($this->subject, $this->getUser()->getIdentity()->case_id, 9);                                 // Zalogování akce
      $this->actionLogModel->commit();
      $this->redirect(':Dashboard:Default:default');
   }
   
   /**
    * Kontrola, zda je datum přípustné.
    * 
    * Musí se jednat o pracovní den a nemůže být dnešek ani nemůže jít do minulosti.
    * Pokud je po 18hodině, tak nemůže být ani zítřek.
    * 
    * @param \Datetime $date
    * @param string $action Akce pro kterou se kontroluje, zda je datum přípustné (hovor, osobní návštěva..)
    * @return boolean true při splnění všech podmíněk výše. False v opačném případě.
    */
   protected function isDatePermissible(\Datetime $date, $action) {
       if(!WorkDayChecker::isWorkDay($date)) return false;
       
       // aktuální datum
       $today = new \DateTime();
       $currentHour = $today->format('H');
       $today->setTime(0, 0, 0);
       
       // rozdíl aktuálního a zadaného data
       $diff = $today->diff($date);
       $diffDays = (integer)$diff->format( "%R%a" );
       
       $criticalHour = $this->paramConfig[$action]['criticalHour'];      // Kritická hodině, po níž již není možné požadavek vyřídit v $beforeCriticalHour
       $beforeCriticalHour = $this->paramConfig[$action]['beforeCriticalHour']; // Den, kdy nejdříve lze uskutečnit hovor (1=zítra, 2=pozítří..)
       $afterCriticalHour = $this->paramConfig[$action]['afterCriticalHour'];  // Den, kdy nejdříve lze uskutečnit hovor, pokud je dnes již po kritické hodině (1=zítra, 2=pozítří..)
       
       // Kontrola, zda datum není menší, než $beforeCriticalHour (=jenbližší den, kdy je možné požadavek vyřídit)
       if( $diffDays < $beforeCriticalHour ) return false;
       
       // Pokud je po kritické hodině, tak nemůže být datum menší než $afterCriticalHour
       if( $currentHour >= $criticalHour && $diffDays < $afterCriticalHour ) return false;
       
       return true;
   }
   
   /**
    * Akce - Zavole mi zpět Zrušení akce
    * @param Nette\Forms\Controls\SubmitButton $button Submit button formuláře
    */
   public function submitCallMeBack(\Nette\Forms\Controls\SubmitButton $button)
   {
      $this->redirect(':Dashboard:Default:default');
   }
   
   /**
    * Akce - Osobní návštěva
    */
   public function actionPersonalVisit()
   {
      $this->template->form = $this->smsForm->personalVisit($this->subject, 'submitPersonalVisit', 'submitPersonalVisitBack');
      
      if( isset($this->paramConfig['priceList']['personalVisit']) 
               && $this->paramConfig['priceList']['personalVisit'][0] > 0 )
       {
           $this->template->price = $this->paramConfig['priceList']['personalVisit'];
       }
   }
   
   /**
    * Akce - Osobní návštěva - Vlastní realizace akce
    * @param Nette\Forms\Controls\SubmitButton $button Submit button formuláře
    */
   public function submitPersonalVisit(\Nette\Forms\Controls\SubmitButton $button)
   {
      $data = $button->getForm()->getValues();
      $address = $data['address'];
      if ($address == NULL)
      {
          $message = $this->translator->translate('Není vybrána žádná adresa');                            // Přidání chyby do formuláře
          $button->getForm()->addError($message);
          return FALSE;
      }
      if ($data['date'] === "")
      {
          $message = $this->translator->translate('Není zadáno datum návštěvy');                                           // Přidání chyby do formuláře
          $button->getForm()->addError($message);
          return FALSE;
      }
      $date = \DateTime::createFromFormat ( "d.m.Y" , $data['date']);
      if ($date === false)
      {
          $message = $this->translator->translate('Chybný tvar data návštěvy');                                           // Přidání chyby do formuláře
          $button->getForm()->addError($message);
          return FALSE;
      }
      if(!$this->isDatePermissible($date, self::DATE_ACTION_VISIT))
      {
          $message = $this->translator->translate('Datum hovoru musí být pracovní den');                                           // Přidání chyby do formuláře
          $button->getForm()->addError($message);
          return FALSE;
      }
      
      /* @var $this->taskModel TaskModel */
      $this->taskModel->addVisit(14, $date, 41);
       
      $this->actionLogModel->begin();
      $this->actionLogModel->insert($this->subject, $this->getUser()->getIdentity()->case_id, 9);                                 // Zalogování akce
      $this->actionLogModel->commit();
      $this->redirect(':Dashboard:Default:default');
   }
   
   /**
    * Akce - Osobní návštěva Zrušení akce
    * @param Nette\Forms\Controls\SubmitButton $button Submit button formuláře
    */
   public function submitPersonalVisitBack(\Nette\Forms\Controls\SubmitButton $button)
   {
      $this->redirect(':Dashboard:Default:default');
   }
}
