<?php
/**
 * WebReminder - AKLH
 *
 * Last revison: 20.6.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Presenter pro práci se SMS
 */


namespace DashboardModule;

use \Nette\Mail\Message;



class PaymentPlanPresenter extends \BaseModule\LoginPresenter
{

   public $smsForm;
   public $paymentPlanModel;
   public $debtModel;
   protected $subject;
    
   
   public function startup()
   {
      parent::startup();
      $this->debtModel = new \DashboardModule\DebtModel($this->db2);
      $this->paymentPlanModel = new \DashboardModule\PaymentPlanModel($this->db2);
      
      $this->smsForm = new SmsForm($this);
      $this->subject = $this->getUser()->getIdentity()->subject_id;
   }
   
   /**
    * Akce - Splátkový kalendář
    */
   public function actionPaymentPlan()
   {
       $settings = $this->paymentPlanModel->getPlanSettings($caseId = 14);
       
       $form = $this->smsForm->paymentPlan('submitPaymentPlan', 'submitPaymentPlanBack', $settings['minimum_payment_count'], $settings['maximum_payment_count'], $settings['minimum_payment_amount']);
       $this->template->form = $form;
       /* @var $form \Nette\Application\UI\Form */
       
       $subjectId = $this->getUser()->getIdentity()->subject_id;
       $caseId = $this->getUser()->getIdentity()->case_id;
       
       $this->template->minPaymentCount = $settings['minimum_payment_count'];
       $this->template->maxPaymentCount = $settings['maximum_payment_count'];
       $this->template->minPaymentValue = $settings['minimum_payment_amount'];
       $this->template->dataDebt = $this->debtModel->show($subjectId, $caseId);
       
       $this->template->lastPaymentValue = '---';
       $this->template->lastPaymentDate = "---";
//       $this->paymentPlanModel->getPlanSettings(13);
   }
   
   /**
    * Akce - Splátkový kalendář - Vlastní realizace akce
    * @param Nette\Forms\Controls\SubmitButton $button Submit button formuláře
    */
   public function submitPaymentPlan(\Nette\Forms\Controls\SubmitButton $button)
   {
       $data = $button->getForm()->getValues();
       
       $date = \DateTime::createFromFormat ( "d.m.Y" , $data['first_payment_date']);
       if ($date === false)
       {
          $message = $this->translator->translate('Chybný tvar data první splátky');                                           // Přidání chyby do formuláře
          $button->getForm()->addError($message);
          return FALSE;
       }
       
       $subjectId = $this->getUser()->getIdentity()->subject_id;
       $caseId = $this->getUser()->getIdentity()->case_id;
       $dataDebt = $this->debtModel->show($subjectId, $caseId);
       
       $payments = $this->paymentPlanModel->generatePaymentPlan($dataDebt['left_to_pay'], $date, $data['payment_value'], $data['total_payments'], $data['maturity_day']);
       $this->paymentPlanModel->savePaymentPlan($caseId = 13, $data['payment_value'], $date, $data['maturity_day'], $payments);
       
       $this->redirect(':Dashboard:Default:default');
   }
   
   /**
    * Akce - Splátkový kalendář Zrušení akce
    * @param Nette\Forms\Controls\SubmitButton $button Submit button formuláře
    */
   public function submitPaymentPlanBack(\Nette\Forms\Controls\SubmitButton $button)
   {
      $this->redirect(':Dashboard:Default:default');
   }
   
   public function handleCalculatePaymentPlan($calculation_type, $total_payments, $payment_value, $first_payment_date, $maturity_day)
   {
       $subjectId = $this->getUser()->getIdentity()->subject_id;
       $caseId = $this->getUser()->getIdentity()->case_id;
       
        $data = $this->debtModel->show($subjectId, $caseId);
        $debt = $data['left_to_pay'];

        $settings = $this->paymentPlanModel->getPlanSettings($caseId = 14);
        
        $lastPaymentValue = "---";
        $lastPaymentDate = "---";
       
        switch($calculation_type)
        {
            case 'total_payments': 
                list( $lastPaymentValue, $paymentCount ) 
                    = $this->calculateByTotalPayments($debt, $total_payments);
                break;
            case 'payment_value': 
                list( $lastPaymentValue, $paymentCount ) 
                    = $this->calculateByPaymentValue($debt, $payment_value);
                break;
        }
        
        /* @var $date \DateTime */
        $date = \DateTime::createFromFormat( "d.m.Y" , $first_payment_date);
        if( $paymentCount > 0 && $date !== false )
        {
            $months = $paymentCount - 1;
            $this->paymentPlanModel->addMonthsToDate($date, $months);
            
            if( $maturity_day !== null && $maturity_day !== "" )
            {
                $this->paymentPlanModel->setDayToDate($date, $maturity_day);
            }
            $lastPaymentDate = $date->format("d.m.Y");
        }

        $this->template->lastPaymentValue = $lastPaymentValue;
        $this->template->lastPaymentDate = $lastPaymentDate;
        $this->redrawControl('info');
   }
   
   protected function calculateByTotalPayments($debt, $total_payments)
   {
        if( $total_payments > 0 )
        {
            $paymentValue = round($debt / $total_payments);
            $lastPaymentValue = $debt - ($paymentValue * ($total_payments - 1));
        
            $this->template->form['payment_value']->setDefaultValue($paymentValue);
            $this->redrawControl('paymentValue');
        }
        
        return array( $lastPaymentValue, $total_payments );
   }
   
   protected function calculateByPaymentValue($debt, $payment_value)
   {
       // v případě, že byla zadána splátka větší než je dluh, se vezme dluh 
       $paymentValue = min($payment_value, $debt);
       
       if( $paymentValue > 0 )
        {
            $paymentCount = ceil($debt / $paymentValue);
            $lastPaymentValue = $debt - ($paymentValue * ($paymentCount - 1));
            
            $this->template->form['total_payments']->setDefaultValue($paymentCount);
            $this->redrawControl('totalPayments');
        }
        
        return array( $lastPaymentValue, $paymentCount );
   }
   
}
