<?php
/**
 * WebReminder - AKLH
 *
 * Last revison: 20.6.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Presenter pro práci se SMS
 */


namespace DashboardModule;

use \Nette\Mail\Message;



class StatementPresenter extends \BaseModule\LoginPresenter
{

   public $smsForm;
   public $fileModel;
   protected $subject;
   protected $taskModel;
   protected $noteModel;
    
   
   public function startup()
   {
      parent::startup();
      $this->taskModel = new \DashboardModule\TaskModel($this->db2);
      $this->fileModel = new \DashboardModule\FileModel($this->db2);
      $this->noteModel = new \DashboardModule\NoteModel($this->db2);
      
      $this->smsForm = new SmsForm($this);
      $this->subject = $this->getUser()->getIdentity()->subject_id;
   }
   
   /**
    * Akce - Vyjádření k případu
    */
   public function actionStatement()
   {
       $form = $this->smsForm->statement($this->subject, 'submitStatement', 'submitStatementBack');
       $this->template->form = $form;
   }
   
   /**
    * Akce - Vyjádření k případu - Vlastní realizace akce
    * @param Nette\Forms\Controls\SubmitButton $button Submit button formuláře
    */
   public function submitStatement(\Nette\Forms\Controls\SubmitButton $button)
   {
       $form = $button->getForm();
       $data = $form->getValues();
       if ($data['type'] === null)
        {
            $message = $this->translator->translate('Není vybrán typ souboru.');                                           // Přidání chyby do formuláře
            $button->getForm()->addError($message);
            return FALSE;
        }
       
       // maximální číslo řádku
       $max = $form->getHttpData($form::DATA_LINE, 'duplicable-max-row-number');
       $caseId = $this->getUser()->getIdentity()->case_id;
       $caseId = 14;
       
       $fileCount = 0;
       // projití všech zduplikovaných inputů a uložení souborů
       for($i = 1; $i <= $max; $i++)
       {
            $fileName = $form->getHttpData($form::DATA_LINE, "file-$i-name");
            $tmpFileName = $form->getHttpData($form::DATA_LINE, "file-$i-id");
            
            // pokud nejsou inputy vyplněny, tak přejít k dalším
            if($tmpFileName === null || $fileName === null)
            {
                continue;
            }
            $fileCount++;
            $fileId = $this->fileModel->insertFile($caseId, $data['type'], $fileName, $data['description']);
            
            $from = $this->paramConfig['dirTmp'].DIRECTORY_SEPARATOR.$tmpFileName;
            $to = $this->paramConfig['collectoraFilePath'].DIRECTORY_SEPARATOR.'0000'.DIRECTORY_SEPARATOR.$fileId;
            rename($from, $to);
       }
       
       if ($fileCount === 0)
        {
            $message = $this->translator->translate('Není vybrán žádný soubor.');                                           // Přidání chyby do formuláře
            $button->getForm()->addError($message);
            return FALSE;
        }
       
       $this->noteModel->insertNote($caseId, $this->translator->translate('Přidáno vyjádření dlužníka'), $this->translator->translate('Přidáno vyjádření dlužníka'));
       
       $this->taskModel->addUserTask($caseId, new \Datetime(), $this->translator->translate('Přidáno vyjádření dlužníka'));
       
       $this->redirect(':Dashboard:Default:default');
   }
   
   /**
    * Akce - Vyjádření k případu Zrušení akce
    * @param Nette\Forms\Controls\SubmitButton $button Submit button formuláře
    */
   public function submitStatementBack(\Nette\Forms\Controls\SubmitButton $button)
   {
      $this->redirect(':Dashboard:Default:default');
   }
}
