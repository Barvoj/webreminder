<?php
/**
 * WebReminder - AKLH
 *
 * Last revison: 4.5.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Presenter pro práci se SMS
 */


namespace DashboardModule;

use \Nette\Mail\Message;



class SmsPresenter extends \BaseModule\LoginPresenter
{

   public $loginModel;
   public $smsForm;
   protected $subject;
   protected $actionLogModel;
   protected $contactServiceModel;
   protected $taskModel;
    
   
   public function startup()
   {
      parent::startup();
      $this->loginModel = new \SignModule\LoginModel($this->db2, $this->translator);
      $this->actionLogModel = new LogModel($this->db1);
      $this->contactServiceModel = new \DashboardModule\ContactServiceModel($this->db2);
      $this->taskModel = new \DashboardModule\TaskModel($this->db2);
      
      $this->smsForm = new SmsForm($this);
      $this->subject = $this->getUser()->getIdentity()->subject_id;
   }
   
   
   /**
    * Akce - Zaslání SMS s platebními symboly
    */
   public function actionPaymentSymbols()
   {
      $this->template->form = $this->smsForm->paymentSymbol($this->subject, 'submitPaymentSymbols', 'submitPaymentSymbolsBack');
      
      if( isset($this->paramConfig['priceList']['paymentSymbols']) 
               && $this->paramConfig['priceList']['paymentSymbols'][0] > 0 )
       {
           $this->template->price = $this->paramConfig['priceList']['paymentSymbols'];
       }
   }

   /**
    * Akce - Zaslání SMS s platebními symboly - Vlastní realizace akce
    * @param Nette\Forms\Controls\SubmitButton $button Submit button formuláře
    */
   public function submitPaymentSymbols(\Nette\Forms\Controls\SubmitButton $button)
   {
      $data = $button->getForm()->getValues();
      $medium = $data['medium'];
      
      switch($medium){
          case 'email':
              $this->emailPaymentSymbols($button);
              break;
          case 'sms':
              $this->smsPaymentSymbols($button);
              break;
      }
      $this->redirect(':Dashboard:Default:default');
   }
   
   protected function smsPaymentSymbols(\Nette\Forms\Controls\SubmitButton $button)
   {
      $data = $button->getForm()->getValues();
      $phone = $data['phone_new'] == NULL ? $data['phone'] : $data['phone_new'];
      if ($phone == NULL)
      {
          $message = $this->translator->translate('Není zadáno nebo vybráno žádné telefonní spojení');                            // Přidání chyby do formuláře
          $button->getForm()->addError($message);
          return FALSE;
      }
      $phoneValid = \BaseModule\Validator::phoneNumber($phone);
      if ($phoneValid === FALSE)
      {
         $button->getForm()->addError($this->translator->translate('Chybný tvar telefonního čísla'));                             // Přidání chyby do formuláře
         return FALSE;
      }
      $message = 'Platební symboly.';
      $case = $this->getUser()->getIdentity()->case_id;
      
      $this->taskModel->sendSms();
      
      if ($data['phone_new'] != NULL)                                                                                             // Zápis nového telefonního čísla (pokud bylo zadáno)
      {
         $this->contactServiceModel->begin();
         $this->contactServiceModel->insertPhone($this->subject, $data['phone_new']);                                                         // Zápis kontaktu do DB
         $this->contactServiceModel->commit();
      }
      $this->actionLogModel->begin();
      $this->actionLogModel->insert($this->subject, $this->getUser()->getIdentity()->case_id, 6);    // Zalogování přihlášení do akcí dlužníka
      $this->actionLogModel->commit();
   }
   
   protected function emailPaymentSymbols(\Nette\Forms\Controls\SubmitButton $button)
   {
      $data = $button->getForm()->getValues();
      $email = $data['email_new'] == NULL ? $data['email'] : $data['email_new'];
      if ($email == NULL)
      {
          $message = $this->translator->translate('Není zadán nebo vybrán žádný e-mail');                            // Přidání chyby do formuláře
          $button->getForm()->addError($message);
          return FALSE;
      }
      $emailValid = \BaseModule\Validator::email($email);
      if ($emailValid === FALSE)
      {
         $button->getForm()->addError($this->translator->translate('Chybný tvar e-mailu'));                             // Přidání chyby do formuláře
         return FALSE;
      }
      $message = 'Platební symboly.';
      $case = $this->getUser()->getIdentity()->case_id;
      
      $this->taskModel->sendEmail();
      
      if ($data['email_new'] != NULL)                                                                                 // Zápis nového emailu (pokud bylo zadáno)
      {
         $this->contactServiceModel->begin();
         $this->contactServiceModel->insertEmail($this->subject, $data['email_new']);                                   // Zápis kontaktu do DB
         $this->contactServiceModel->commit();
      }
      $this->actionLogModel->begin();
      $this->actionLogModel->insert($this->subject, $this->getUser()->getIdentity()->case_id, 6);    // Zalogování přihlášení do akcí dlužníka
      $this->actionLogModel->commit();
   }

   
   /**
    * Akce - Zaslání SMS s platebními symboly - Zrušení akce
    * @param Nette\Forms\Controls\SubmitButton $button Submit button formuláře
    */
   public function submitPaymentSymbolsBack(\Nette\Forms\Controls\SubmitButton $button)
   {
      $this->redirect(':Dashboard:Default:default');
   }
   
}
