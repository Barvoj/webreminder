<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 25.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 */


namespace DashboardModule;


class DefaultPresenter extends \BaseModule\LoginPresenter
{

   private $subjectModel;
   private $caseModel;
   private $debtModel;
   private $paymentModel;
   private $scheduleModel;
   private $fileModel;
   private $transactionModel;
   private $contactModel;
   private $descriptionModel;
   protected $logModel;  // ??
   private $companyModel;
    
   
   public function startup()
   {
      parent::startup();
      $this->subjectModel = new SubjectModel($this->db1);
      $this->caseModel = new CaseModel($this->db1);
      $this->paymentModel = new PaymentModel($this->db1);
      $this->debtModel = new DebtModel($this->db1);
      $this->scheduleModel = new ScheduleModel($this->db1);
      $this->fileModel = new FileModel($this->db1);
      $this->transactionModel = new TransactionModel($this->db1);
      $this->contactModel = new ContactModel($this->db1);
      $this->descriptionModel = new DescriptionModel($this->db1);
      $this->logModel = new LogModel($this->db1);
      $this->companyModel = new CompanyModel($this->db1);
   }

   
   /**
    * Akce - Zobrazení úvodní přehledové stránky
    */
   public function actionDefault()
   {
      $subject = $this->getUser()->getIdentity()->subject_id;
      $case = $this->getUser()->getIdentity()->case_id;
      $this->template->case = $case;
      $this->template->cases = $this->getUser()->getIdentity()->cases;
      $this->template->dataSubject = $this->subjectModel->show($subject);
      $this->template->dataCase = $this->caseModel->show($subject, $case);
      $this->template->dataDebt = $this->debtModel->show($subject, $case);
      $this->template->dataPayment = $this->paymentModel->show($subject, $case);
      $this->template->dataSchedule = $this->scheduleModel->show($subject, $case);
      $this->template->dataFile = $this->fileModel->show($subject, $case);
      $this->template->dataDescription = $this->descriptionModel->show($subject, $case);
      $this->template->dataTransaction = $this->transactionModel->showCase($case);
      $this->template->dataContact = $this->contactModel->showCase($subject);
      $this->template->dataLog = $this->logModel->showSubject($subject);
      $this->template->dataCompany = $this->companyModel->show();
   }


   /**
    * Akce - Změna případu
    */
   public function actionChange($case)
   {
      $this->getUser()->getIdentity()->case_id = $case;                                                                           // Zapsání Id nového případu do SESSION
      $this->forward(':Dashboard:Default:default');                                                                               // Skok na zobrazení
   }

}
