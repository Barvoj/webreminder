<?php
/**
 * WebReminder - AKLH
 *
 * Last revison: 24.6.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Presenter pro práci se SMS
 */


namespace DashboardModule;

use \Nette\Mail\Message;
use Nette\Http\IResponse;

class FilePresenter extends \BaseModule\LoginPresenter
{

   public $paymentModel;
   public $loginModel;
   public $smsModel;
   public $smsForm;
   protected $subject;
   protected $actionLogModel;
   protected $contactServiceModel;
   protected $taskModel;
    
   
   public function startup()
   {
      parent::startup();
      $this->paymentModel = new PaymentModel($this->db2);
      $this->loginModel = new \SignModule\LoginModel($this->db2, $this->translator);
      $this->smsModel = new \BaseModule\Sms();
      $this->smsForm = new SmsForm($this);
      $this->subject = $this->getUser()->getIdentity()->subject_id;
      $this->actionLogModel = new LogModel($this->db1);
      $this->contactServiceModel = new \DashboardModule\ContactServiceModel($this->db2);
      $this->taskModel = new \DashboardModule\TaskModel($this->db2);
   }
   
   /**
    * Zpracování uploadu souboru po částech (tzv. chunk) - na straně JS knihovna Resumable.js
    * 
    * Upload každého chunk má dvě fáze:
    * 1.) GET - kterým si Resumable.js ověřuje, zda již není chunk na serveru uložen (vrací 200 pokud již na serveru je a 204 pokud není)
    * 2.) POST - kterým se uploadne chung na server (pokud GET vrací 204)
    */
   public function handleUpload()
   {
       $method = $this->getHttpRequest()->getMethod();
       
       switch($method){
           case 'GET':
               $this->checkIfChunkAlreadyExists();
               break;
           case 'POST':
               $this->saveChunkAndCreateFileIfComplete();
               break;
       }
       $this->sendPayload();
   }
   
   /**
    * Podle toho, zda již chunk existuje, nastaví status kód 200 (existuje) nebo 204 (neexistuje)
    */
   protected function checkIfChunkAlreadyExists()
   {
       if( $this->chunkExists() )
        {
            $this->getHttpResponse()->setCode( IResponse::S200_OK );
        }
        else
        {
            $this->getHttpResponse()->setCode( IResponse::S204_NO_CONTENT );
        }
   }
   
   /**
    * Kontrola, zda chunk již na serveru existuje
    * @return boolean
    */
   protected function chunkExists()
    {
        $httpRequest = $this->getHttpRequest();

        $chunkNumber = $httpRequest->getQuery('resumableChunkNumber');
        $identifier = $httpRequest->getQuery('resumableIdentifier');
        $fileName = $httpRequest->getQuery('resumableFilename');
        
        $name = $fileName . ".part" . $chunkNumber;
        $dirName = $this->paramConfig['dirTmp'] . DIRECTORY_SEPARATOR . $identifier;
        $file = $dirName . DIRECTORY_SEPARATOR . $name;
        return $this->fileExists( $file );
    }
    
    /**
     * Kontrola existence souboru
     * @param type $file
     * @return boolean
     */
    protected function fileExists( $file )
    {
            return file_exists( $file ) && is_file( $file );
    }
    
    protected function saveChunkAndCreateFileIfComplete()
    {
        if( $this->saveChunk() )
        {
            $this->getHttpResponse()->setCode( IResponse::S200_OK );

            $filename = $this->createFileIfComplete();
            

            if( $filename !== false )
            {
                $this->payload->fileName = basename($filename);
            }
        }
        else
        {
            $this->getHttpResponse()->setCode( IResponse::S400_BAD_REQUEST );
        }
    }

    /**
     * Uloží uploadnutý chunk.
     * @return boolean
     * @throws IOException
     */
    protected function saveChunk()
    {
        $httpRequest = $this->getHttpRequest();
        $chunk = $httpRequest->getFile( 'file' );

        if( isset($chunk) && $chunk->isOk() )
        {
            $chunkNumber = $httpRequest->getPost('resumableChunkNumber');
            $identifier = $httpRequest->getPost('resumableIdentifier');
            $fileName = $httpRequest->getPost('resumableFilename');

            $name = $fileName . ".part" . $chunkNumber;
            $dirName = $this->paramConfig['dirTmp'] . DIRECTORY_SEPARATOR . $identifier;
            if( !$this->directoryExists( $dirName ) )
            {
                $this->makeDir( $dirName );
            }
            $file = $dirName . DIRECTORY_SEPARATOR . $name;

            try
            {
                $chunk->move( $file );
            }
            catch(InvalidStateException $ex)
            {
                throw new IOException( "Can not write file `{$file}`." );
            }

            return true;
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Kontrola existence složky
     * @param type $directory
     * @return boolean
     */
    protected function directoryExists( $directory )
    {
            return file_exists( $directory ) && is_dir( $directory );
    }
    
    /**
     * Vytvoří složku, pokud neexistuje
     * @param type $directory
     * @param type $recursive Mají se rekurzivně vytvořit všechny složky zadané cesty?
     * @throws IOException
     */
    protected function makeDir( $directory, $recursive = true )
    {
            $res = mkdir( $directory, 0777, $recursive );

            if( !$res )
            {
                    throw new IOException( "Can not create directory `{$directory}`." );
            }
    }
    
    /**
     * Pokud již jsou všechny chunk na serveru, vytvoří z nich původní soubor
     * @return string Jméno vytvořeného souboru nebo false, pokud ještě nebylo možné soubor vytvořit
     */
    protected function createFileIfComplete()
    {
        $httpRequest = $this->getHttpRequest();

        $identifier = $httpRequest->getPost('resumableIdentifier');
        $fileName = $httpRequest->getPost('resumableFilename');
        $totalChunks = intval( $httpRequest->getPost('resumableTotalChunks') );

        $complete = $this->isFileComplete($identifier, $fileName, $totalChunks);

        if( $complete )
        {
            return $this->createFileFromChunks( $identifier, $fileName, $totalChunks);
        }
        else
        {
            return false;
        }
    }
    
    /**
     * Kontrola, zda již byly všechny chunk nahrány na server
     * @param string $identifier Unikátní identifikátor souboru (generovaný knihovnou Resumable.js)
     * @param string $fileName Jméno souboru
     * @param int $totalChunks Celkový počet chunks
     * @return boolean
     */
    public function isFileComplete( $identifier, $fileName, $totalChunks )
    {
        $count = 0;
        $dirName = $this->paramConfig['dirTmp'] . DIRECTORY_SEPARATOR . $identifier;
        $finder = \Nette\Utils\Finder::findFiles("$fileName.part*")->in($dirName);
        foreach ( $finder as $key => $file )
        {
            $count++;
        }
        return $count === $totalChunks;
    }
    
    /**
     * Spojení všech chunk zpět do původního souboru
     * @param string $identifier Unikátní identifikátor souboru (generovaný knihovnou Resumable.js)
     * @param string $fileName Jméno souboru
     * @param int $chunkNumber Celkový počet chunks
     * @return string Název souboru
     * @throws IOException
     */
    public function createFileFromChunks( $identifier, $fileName, $chunkNumber )
    {
        $chunkDir = $this->paramConfig['dirTmp'] . DIRECTORY_SEPARATOR . $identifier;
        $chunk = $chunkDir . DIRECTORY_SEPARATOR . $fileName;

        $dir = $this->paramConfig['dirTmp'];

        if( !$this->directoryExists( $dir ) )
        {
            $this->makeDir( $dir );
        }

        $file = $dir . DIRECTORY_SEPARATOR . $fileName;
        $file = tempnam($dir, 'gaw');

        if( ( $fp = fopen( $file, 'w' ) ) !== false ) {
            for ( $i = 1; $i <= $chunkNumber; $i++ ) {
                fwrite( $fp, file_get_contents( $chunk . '.part' . $i ) );
            }
            fclose($fp);
        } else {
            throw new IOException("Cannot create video file '$file'.");
        }

        if ( rename( $chunkDir, $chunkDir.'_UNUSED' ) ) {
            \Nette\Utils\FileSystem::delete($chunkDir.'_UNUSED');
        } else {
            \Nette\Utils\FileSystem::delete($chunkDir);
        }
        
        return $file;
    }
}
