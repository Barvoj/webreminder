<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 25.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základ DB modelů
 * 
 */

namespace DashboardModule;
 

class ScheduleModel extends \BaseModule\DbModel
{

   /**
    * Výpis souborů případu
    * @param int $subjectId Id subjektu
    * @param int $caseId Id případu
    * @return array Pole záznamů
    */
   public function show($subjectId, $caseId)                                                                                               // 
   {
      $query = "SELECT A.subject_id, A.case_id, A.currency_label,
                       A.amount, A.count_installment, A.first_installment, A.next_installment, A.next_installment_date_f
                FROM ( SELECT 1 subject_id, 1 case_id, 'Kč' currency_label,
                              28500 amount, 10 count_installment, 2850 first_installment, 2850 next_installment,
                              '10.12.2014' next_installment_date_f ) A
                WHERE subject_id = %iN AND case_id = %iN LIMIT 1";
      return $this->db->fetch($query, $subjectId, $caseId);
   }
   
}
