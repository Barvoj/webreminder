<?php
/**
 * WebReminder - AKLH
 *
 * Last revison: 9.4.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * DB model pro zobrazení kontaktů
 * 
 */

namespace DashboardModule;
 

class ContactServiceModel extends \BaseModule\DbModel
{
   

   /**
    * Uložení kontaktu typu telefon
    * @param int $subjectId Id subjektu
    * @return array Pole záznamů
    */
   public function insertPhone($subjectId, $phone)                                                                                               // 
   {
      $query = "INSERT INTO vp_contacts
                       (vp_contact_valid_from, vp_contact_valid_to,
                        vp_setting_check_status_id, vp_setting_validity_type_id, vp_contact_col1)
                VALUES (now(), '2025-01-01', 2, 2, %sN)";
      $this->db->query($query, $phone);
      $contactId = $this->db->insertId();
      $query = "INSERT INTO vp_contacts_types (vp_contact_id, vp_contact_type_id)
                VALUES (%iN, 4)";
      $this->db->query($query, $contactId);
      $query = "INSERT INTO vp_case_subject_contacts (vp_contact_id, vp_case_subject_id, vp_setting_validity_type_id)
                VALUES (%iN, %iN, 2)";
      $this->db->query($query, $contactId, $subjectId);
   }

   
   /**
    * Uložení kontaktu typu e-mail
    * @param int $subjectId Id subjektu
    * @return array Pole záznamů
    */
   public function insertEmail($subjectId, $email)                                                                                               // 
   {
      $query = "INSERT INTO vp_contacts
                       (vp_contact_valid_from, vp_contact_valid_to,
                        vp_setting_check_status_id, vp_setting_validity_type_id, vp_contact_col1)
                VALUES (now(), '2025-01-01', 2, 2, %sN)";
      $this->db->query($query, $email);
      $contactId = $this->db->insertId();
      $query = "INSERT INTO vp_contacts_types (vp_contact_id, vp_contact_type_id)
                VALUES (%iN, 8)";
      $this->db->query($query, $contactId);
      $query = "INSERT INTO vp_case_subject_contacts (vp_contact_id, vp_case_subject_id, vp_setting_validity_type_id)
                VALUES (%iN, %iN, 2)";
      $this->db->query($query, $contactId, $subjectId);
   }

   
   /**
    * Uložení kontaktu typu adresa
    * @param int $subjectId Id subjektu
    * @param string $street Ulice
    * @param string $number Číslo
    * @param string $village Obec
    * @param string $zip PSČ
    * @return array Pole záznamů
    */
   public function insertAddress($subjectId, $street, $number, $village, $zip)                                                                                               // 
   {
      $query = "INSERT INTO vp_contacts
                       (vp_contact_valid_from, vp_contact_valid_to,
                        vp_setting_check_status_id, vp_setting_validity_type_id, 
                        vp_contact_col1, vp_contact_col2, vp_contact_col4, vp_contact_col6)
                VALUES (now(), '2025-01-01', 2, 2, %sN, %sN, %sN, %sN)";
      $this->db->query($query, $street, $number, $village, $zip);
      $contactId = $this->db->insertId();
      $query = "INSERT INTO vp_contacts_types (vp_contact_id, vp_contact_type_id)
                VALUES (%iN, 3)";
      $this->db->query($query, $contactId);
      $query = "INSERT INTO vp_case_subject_contacts (vp_contact_id, vp_case_subject_id, vp_setting_validity_type_id)
                VALUES (%iN, %iN, 2)";
      $this->db->query($query, $contactId, $subjectId);
   }
   
}
