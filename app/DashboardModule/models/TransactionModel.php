<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 21.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základ DB modelů
 * 
 */

namespace DashboardModule;
 

class TransactionModel extends \BaseModule\DbModel
{
   

   /**
    * Výpis souborů případu
    * @param int $case Id případu
    * @return array Pole záznamů
    */
   public function showCase($case)                                                                                               // 
   {
      $query = "SELECT A.case_id, A.sentence, A.type, A.amount, A.currency_label
                FROM ( SELECT 1 case_id, 1 sentence, 'Jistina' type, -28500 amount, 'Kč' currency_label
                       UNION ALL
                       SELECT 1 case_id, 2 sentence, 'Poplatky' type, -1500 amount, 'Kč' currency_label
                       UNION ALL
                       SELECT 1 case_id, 3 sentence, 'Splátky' type, 10000 amount, 'Kč' currency_label
                       UNION ALL
                       SELECT 2 case_id, 1 sentence, 'Jistina' type, -5800 amount, 'Kč' currency_label
                       UNION ALL
                       SELECT 2 case_id, 2 sentence, 'Poplatky' type, -520 amount, 'Kč' currency_label
                       UNION ALL
                       SELECT 2 case_id, 3 sentence, 'Splátky' type, 1500 amount, 'Kč' currency_label ) A
                WHERE A.case_id = %iN
                ORDER BY 1";
      return $this->db->fetchAll($query, $case);
   }
   
}
