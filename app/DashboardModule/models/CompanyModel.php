<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 21.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základ DB modelů
 * 
 */

namespace DashboardModule;
 

class CompanyModel extends \BaseModule\DbModel
{
   

   /**
    * Výpis souborů případu
    * @param int Id případu
    * @return array Pole záznamů
    */
   public function show()                                                                                               // 
   {
      $query = "SELECT '1. Inkasní společnost a.s.' name,
                       'U nábřeží 128/15, Praha 1, 110 00' address,
                       '+420 802 150 001' phone,
                       '+420 802 150 021' fax,
                       'info@1inkasni.cz' email,
                       'http://www.1inkasni.cz' web,
                       '72815247' registration_number,
                       'CZ72815247' vat_number";
      return $this->db->fetch($query);
   }
   
}
