<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 21.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základ DB modelů
 * 
 */

namespace DashboardModule;
 

class LogModel extends \BaseModule\DbModel
{
   

   /**
    * Výpis logů akcí pro daný subjekt
    * @param int $subject Id subjektu
    * @return array Pole záznamů
    */
   public function showSubject($subject)
   {
      $query = "SELECT '06.01.2015 12:52:27' date_f, 'Přihlášen subjekt: Josef Novák' action
                UNION ALL
                SELECT '06.01.2015 12:55:04' date_f, 'Přidán kontakt telefon: +420 758 475' action
                UNION ALL
                SELECT '17.01.2015 09:24:52' date_f, 'Přidána poznámka' action
                UNION ALL
                SELECT '20.02.2015 14:28:05' date_f, 'Přihlášen subjekt: Josef Novák' action
                ORDER BY 1 DESC";
      return $this->db->fetchAll($query);
   }
   
   /**
    * Zapsání logu akce
    * @param int $subjectId Id subjektu
    * @param int $caseId Id případu
    * @param int $actionId Id akce
    * @return int Id záznamu
    */
   public function insert($subjectId, $caseId, $actionId)
   {
//      $query = 'INSERT INTO action_log (subject_id, case_id, action_id) VALUES (%iN, %iN, %iN)';
//      $this->db->query($query, $subjectId, $caseId, $actionId);
//      return $this->db->insertId();
   }
   
}
