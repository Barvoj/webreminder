<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 23.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základ DB modelů
 * 
 */

namespace DashboardModule;
 

class PaymentModel extends \BaseModule\DbModel
{
   

   /**
    * Výpis souborů případu
    * @param int $subjectId Id subjektu
    * @param int $caseId Id případu
    * @return array Pole záznamů
    */
   public function show($subjectId, $caseId)                                                                                               // 
   {
      $query = "SELECT A.subject_id, A.case_id, A.bank_account, A.swift, A.iban, A.vs, A.ks, A.ss, A.amount, A.currency_label
                FROM ( SELECT 1 subject_id, 1 case_id, '587986/0300' bank_account, 'COSBCZ' swift, 'COSBCZ-587986-CZ' iban,
                              '128987563' vs, '0558' ks, '' ss, 18500 amount, 'Kč' currency_label
                       UNION ALL                              
                       SELECT 1 subject_id, 2 case_id, '714-65487899/0100' bank_account, 'KBCZ' swift, 'KBCZ-714-65487899-CZ' iban,
                              '518685974' vs, '0558' ks, '' ss, 5800 amount, 'Kč' currency_label
                       UNION ALL                              
                       SELECT 1 subject_id, 3 case_id, '128005127/2680' bank_account, 'MBANKCZ' swift, 'MBANK-128005127-CZ' iban,
                              '128574' vs, '0558' ks, '' ss, 8800 amount, 'Kč' currency_label ) A
                WHERE subject_id = %iN AND case_id = %iN";
      return $this->db->fetch($query, $subjectId, $caseId);
   }
   
}
