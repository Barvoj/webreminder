<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 27.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základ DB modelů
 * 
 */

namespace DashboardModule;
 

class ContactModel extends \BaseModule\DbModel
{
   

   /**
    * Výpis souborů případu
    * @param int $case Id případu
    * @return array Pole záznamů
    */
   public function showCase($case)                                                                                               // 
   {
      $query = "SELECT 1 sentence, 'Adresa' type, 'U zeleného stromu 12/874, Praha 6 160 00' content
                UNION ALL
                SELECT 2 sentence, 'E-mail' type, 'josef.novak@seznam.cz' content
                UNION ALL
                SELECT 3 sentence, 'Telefon' type, '+420 787 988 150' content
                ORDER BY 1";
      return $this->db->fetchAll($query);
   }
   
}
