<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 25.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základ DB modelů
 * 
 */

namespace DashboardModule;
 

class DebtModel extends \BaseModule\DbModel
{

   /**
    * Výpis souborů případu
    * @param int $subjectId Id subjektu
    * @param int $caseId Id případu
    * @return array Pole záznamů
    */
   public function show($subjectId, $caseId)                                                                                               // 
   {
      $query = "SELECT A.subject_id, A.case_id, A.date_f, A.currency_label,
                       A.principal, A.original_amount, A.paid, A.left_to_pay
                FROM ( SELECT 1 subject_id, 1 case_id, '12.5.2014' date_f, 'Kč' currency_label,
                              28500 principal, 40000 original_amount, 10000 paid, 2899 left_to_pay
                       UNION ALL                              
                       SELECT 1 subject_id, 2 case_id, '12.5.2014' date_f, 'Kč' currency_label,
                              120000 principal, 150000 original_amount, 30000 paid, 120000 left_to_pay
                       UNION ALL                              
                       SELECT 1 subject_id, 3 case_id, '12.5.2014' date_f, 'Kč' currency_label,
                              8000 principal, 125500 original_amount, 118500 paid, 8000 left_to_pay ) A
                WHERE subject_id = %iN AND case_id = %iN";
      return $this->db->fetch($query, $subjectId, $caseId);
   }
   
}
