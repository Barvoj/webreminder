<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 25.6.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základ DB modelů
 * 
 */

namespace DashboardModule;
 

class PaymentPlanModel extends \BaseModule\DbModel
{
   
    /**
     * Načte nastavení splátkového kalendáře pro zadaný případ.
     * 
     * @param int $caseId Id případu
     * @return array
     */
    public function getPlanSettings($caseId)
    {
        $query = "SELECT pps.* 
            FROM payment_plan_settings pps
            JOIN vp_client_projects USING (vp_client_project_id)
            JOIN vp_cases USING (vp_client_project_id)
            WHERE vp_case_id = %i";
        
        return $this->db->fetch($query, $caseId);
    }
    
    /**
     * Vygeneruje splátkový kalendář
     * 
     * @param int $caseId Id případu
     * @param DateTime $firstPaymentDate Datum první splátky
     * @param float $paymentValue Výše splátky
     * @param int $totalPayments Počet splátek celkem
     * @return int
     */
    public function generatePaymentPlan($debt, $firstPaymentDate, $paymentValue, $totalPayments, $maturityDay)
    {
       $date = clone $firstPaymentDate;
       $payments = array();
       $value = $paymentValue;
       
       for($i = 0; $i < $totalPayments; $i++)
       {
           if( ($i+1) === intval($totalPayments) )
           {
               $value = $debt - $i * $value;
           }
           
           $payments[] = array(
               "code_id" => 88,
               "value" => $value,
               "date" => clone $date
           );
           
           $this->addMonthsToDate($date, 1);
           $this->setDayToDate($date, $maturityDay);
       }
       
       return $payments;
    }
    
    /**
     * Přičte k datu $months měsíců. 
     * 
     * Pokud výsledný měsíc nemá den v měsíci původního data, je den v měsíci 
     * nastaven na maximální možný.
     * 
     * Př.: 30.1.2016 +1 month = 29.2.2016
     * 
     * @param \DateTime $date
     * @param type $months
     */
    public function addMonthsToDate(\DateTime $date, $months)
    {
        $day = $date->format('d');
        // Nastavení na prvního v měsíci, aby nedocházelo k přeskakování
        // krátkých měsícu Př.: 30.1. +1 month = 2.3. (což je mežádoucí)
        $date->modify('first day of this month');
        $date->modify("+$months month");
        $this->setDayToDate($date, $day);
    }
    
    /**
     * Nastaví den datu. 
     * 
     * Pokud měsíc nemá den v měsíci který se má nastavit, je den v měsíci 
     * nastaven na maximální možný.
     * 
     * Př.: 30.2.2016 nastavit nelze => vrací 29.2.2016
     * 
     * @param \DateTime $date
     * @param type $months
     */
    public function setDayToDate(\DateTime $date, $day)
    {
        $maxDayOfMonth = $this->getMaxDayOfMonth($date);
        // Pokud je den větší (Př.: 31) než maximální den v měsíci (Př.: 28)
        // tak se vezme to menší.
        $d = min($day, $maxDayOfMonth);
        $date->setDate($date->format("Y"), $date->format("m"), (int) $d);
    }
    
    /**
     * Vrací maximální možný den měsíce zadaného data.
     * 
     * Př.: pro datum 15.2.2016 vrací 29
     * 
     * @param \DateTime $date
     * @return type
     */
    protected function getMaxDayOfMonth(\DateTime $date)
    {
         $maxDateOfMonth = clone $date;
         // zjištění maximálního data v měsící Př.: 28.2.2015
         $maxDateOfMonth->modify('last day of this month');
         // získání maximálního dne v měsící Př.: 28
         $maxDayOfMonth = $maxDateOfMonth->format("d");

         return $maxDayOfMonth;
    }
    
    /**
     * Uloží splátkový kalendář no DB
     * 
     * @param int $caseId Id případu
     * @param float $paymentValue Výše splátky
     * @param DateTime $firstPaymentDate Datum první splátky
     * @param int $maturityDay Den (v měsíci) ve kterém končí splatnost splátky
     * @param int $payments Pole splátek
     * @return Id splátkového kalendáře
     */
    public function savePaymentPlan($caseId, $paymentValue, $firstPaymentDate, $maturityDay, $payments)
    {
        
        $query = "INSERT INTO payment_plans (starting_date, payment_amount, payment_due_on, created_by, vp_case_id)
                    VALUES(%d, %f, %i, 10, %i)";
        
        $this->db->query($query, $firstPaymentDate, $paymentValue, $maturityDay, $caseId);
        $planId = $this->db->insertId();
        
        $query = "INSERT INTO payment_plans_transactions (payment_plan_id, transaction_value, transaction_date)
                    VALUES(%i, %f, %d)";
        
        foreach($payments as $payment)
        {
            $this->db->query($query, $planId, $payment['value'], $payment['date']);
        }
        
        
        return $planId;
    }
}
