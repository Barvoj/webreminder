<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 23.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základ DB modelů
 * 
 */

namespace DashboardModule;
 

class SubjectModel extends \BaseModule\DbModel
{

   /**
    * Výpis dat subjektu
    * @param int $subjectId Id subjektu
    * @return array Pole záznamů
    */
   public function show($subjectId)
   {
      $query = "SELECT A.id, A.code, A.name, A.type,
                       A.birth_number, A.birth_date_f, A.registration_number
                FROM ( SELECT 1 id, '12815' code, 'Novák Josef' name, 'Dlužník' type,
                              '601022/0012' birth_number, '22.10.1960' birth_date_f, NULL registration_number ) A
                WHERE A.id = %iN";
      return $this->db->fetch($query, $subjectId);
   }
/*      $query = "SELECT S.vp_subject_id id, S.vp_subject_code code, S.vp_subject_name name, 
                       T.subject_type_code type, R.subject_sort_code sort_code, R.subject_sort_description sort_description,
                       S.vp_subject_birth_number, S.vp_subject_dob birth_date, date_output(S.vp_subject_dob) birth_date_f, 
                       S.vp_subject_registration_number registration_number, S.vp_subject_vat_number vat_number,
                FROM   vp_subjects S
                LEFT JOIN subject_types T ON T.subject_type_id = S.subject_type_id
                LEFT JOIN subject_sorts R ON R.subject_sort_id = S.subject_sort_id
                WHERE  S.vp_subject_id = %iN";*/
 
}
