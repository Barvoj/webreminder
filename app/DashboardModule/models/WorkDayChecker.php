<?php

namespace DashboardModule;

/**
 * Třída pro ověřování, zda je zadané datum víkend, svátek či pracovní den.
 */
class WorkDayChecker {
    
    /**
     * Datumy velikonoc. rok => [den, měsíc]
     * @var array 
     */
    protected static $easterDays = [
            2015 => [6,4],
            2016 => [28,3],
            2017 => [17,4],
            2018 => [2,4],
            2019 => [22,4],
            2020 => [13,4],
            2021 => [5,4],
            2022 => [18,4],
            2023 => [10,4],
            2024 => [1,4],
            2025 => [21,4],
            2026 => [6,4],
            2027 => [29,3],
            2028 => [17,4],
            2029 => [2,4],
            2030 => [22,4]
        ];
    
    /**
     * Datumy českých svátků. [den, měsíc, popis]
     * @var array
     */
    protected static $holidays = [
            [1, 1, 'Den obnovy samostatného českého státu'], 
            [1, 5, 'Svátek práce'], 
            [8, 5, 'Den vítězství'],
            [5, 7, 'Den slovanských věrozvěstů Cyrila a Metoděje'], 
            [6, 7, 'Den upálení mistra Jana Husa'], 
            [28, 9, 'Den české státnosti'],
            [28, 10, 'Den vzniku samostatného československého státu'], 
            [17, 11, 'Den boje za svobodu a demokracii'], 
            [24, 12, 'Štědrý den'],
            [25, 12, '1. svátek vánoční'], 
            [26, 22, '2. svátek vánoční']
        ];
    
    /**
     * Kontrola, zda se jedná o Velikonoce
     * @param \Datetime $date
     * @return boolean
     */
    public static function isEaster(\Datetime $date ) {
        $y = $date->format('Y');
        if( isset( self::$easterDays[$y] ) )
        {
            if ($date->format('n') == self::$easterDays[$y][1]
            && $date->format('j') == self::$easterDays[$y][0]) {
                return true;
            }
        }
        return false;
    }
    
    /**
     * Kontrola, zda se jedná o (český) svátek (včetně Velikonoc)
     * @param \Datetime $date
     * @return boolean
     */
    public static function isHoliday(\Datetime  $date ) {
        for ($i = 0; $i < count( self::$holidays ); $i++) {
            if ($date->format('n') == self::$holidays[$i][1]
                    && $date->format('j') == self::$holidays[$i][0]) {
                  return true;
            }
        }
        return self::isEaster($date);
    }
    
    /**
     * Kontrola, zda se jedná o víkend.
     * @param \Datetime $date
     * @return type
     */
    public static function isWeekend(\Datetime $date) {
        $weekDay = $date->format('w');
        return ($weekDay == 0 || $weekDay == 6);
    }
    
    /**
     * Kontrola, zda se jedná o pracovní den (není víkend ani žádný svátek)
     * @param \Datetime $date
     * @return boolean
     */
    public static function isWorkDay(\Datetime $date) {
        if( self::isWeekend( $date ) ) {
            return false;
        } else if( self::isHoliday( $date ) ) {
            return false;
        }
        return true;
    }
}
