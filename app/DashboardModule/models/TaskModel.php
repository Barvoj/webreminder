<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 22.6.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základ DB modelů
 * 
 */

namespace DashboardModule;

class TaskModel extends \BaseModule\DbModel {
    
    const SMS_TASK_ID = 3,
            EMAIL_TASK_ID = 5,
            USER_TASK_ID = 4;
    
    public function sendSms($caseId = 14, $taskTemplateId = 6, $targetTableId = 40) {
        $query = "INSERT INTO task_log (task_id, task_template_id, created_by, target_table_name, target_table_id, planned, planned_for)
                  VALUES (%i, %i, 10, 'vp_contacts', %i, NOW(), 10)";
        $this->db->query($query, self::SMS_TASK_ID, $taskTemplateId, $targetTableId);
        $taskId = $this->db->getInsertId();
        
        $query = "INSERT INTO case_tasks (vp_case_id, task_log_id) VALUES (%s, %s)";
        $this->db->query($query, $caseId, $taskId);
    }
    
    public function sendEmail($caseId = 14, $taskTemplateId = 4, $targetTableId = 40) {
        $query = "INSERT INTO task_log (task_id, task_template_id, created_by, target_table_name, target_table_id, planned, planned_for)
                  VALUES (%i, %i, 10, 'vp_contacts', %i, NOW(), 10)";
        $this->db->query($query, self::EMAIL_TASK_ID, $taskTemplateId, $targetTableId);
        $taskId = $this->db->getInsertId();
        
        $query = "INSERT INTO case_tasks (vp_case_id, task_log_id) VALUES (%s, %s)";
        $this->db->query($query, $caseId, $taskId);
    }
    
    public function addUserTask($caseId, \DateTime $datetime, $description = "") {
        $query = "INSERT INTO task_log (task_id, task_template_id, created_by, target_table_name, target_table_id, task_description, planned, planned_for)
                  VALUES (%i, 0, 10, 'vp_cases', %i, %s, %t, %i)";
        $this->db->query($query, self::USER_TASK_ID, $caseId, $description, $datetime, $this->getCaseUser($caseId));
        $taskId = $this->db->getInsertId();
        
        $query = "INSERT INTO case_tasks (vp_case_id, task_log_id) VALUES (%s, %s)";
        $this->db->query($query, $caseId, $taskId);
    }
    
    protected function getCaseUser($caseId) {

        $sql = "SELECT vp_users.vp_user_id 
            FROM case_users
            INNER JOIN vp_users USING (vp_user_id)
            WHERE vp_case_id = %s
            AND NOW() BETWEEN valid_from AND valid_to";
        return $this->db->query($sql, $caseId)->fetchSingle();
    }
    
    public function addVisit($caseId, \DateTime $date, $contactId) {
        $query = "INSERT INTO `debtor_visits` (`vp_case_id`, `vp_user_id`, `vp_subject_id`, `vp_contact_id`, `debtor_visit_date`, `visit_status_id`, `visit_payment_notice`, `visit_debtor_status_id`)
            VALUES (%i, %i, %i, %i, %d, '0', 1, '0');";
        $this->db->query($query, $caseId, $this->getCaseUser($caseId), 27, $contactId, $date);
    }
}

