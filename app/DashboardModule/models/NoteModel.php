<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 25.6.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základ DB modelů
 * 
 */

namespace DashboardModule;
 

class NoteModel extends \BaseModule\DbModel
{
    public function insertNote($caseId, $title, $note)
    {
        $query = "INSERT INTO vp_notes(vp_user_id, note_access_id, vp_note_title, vp_note_text)
                    VALUES(%i, %i, %s, %s)";
        $this->db->query($query, 10, 1, $title, $note);
        $noteId = $this->db->insertId();
        
        $query = "INSERT INTO vp_case_notes (vp_case_id, vp_note_id)
                    VALUES(%i, %i)";
        $this->db->query($query, $caseId, $noteId);
    }
}
