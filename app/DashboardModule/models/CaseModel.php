<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 23.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základ DB modelů
 * 
 */

namespace DashboardModule;
 

class CaseModel extends \BaseModule\DbModel
{

   /**
    * Výpis dat případu
    * @param int $subjectId Id subjektu
    * @param int $caseId Id případu
    * @return array Pole záznamů
    */
   public function show($subjectId, $caseId)                                                                                               // 
   {
      $query = "SELECT A.subject_id subject_id,  A.case_id case_id, A.case_code case_code, A.client_name client_name, 
                       A.client_case_code client_case_code, A.status, A.date_f date_f
                FROM ( SELECT 1 subject_id, 1 case_id, '587968' case_code, 'ČSOB Leasing a.s.' client_name, 
                              'XB12812' client_case_code, 'Stav' status, '12.5.2014' date_f
                       UNION ALL
                       SELECT 1 subject_id, 2 case_id, '712054' case_code, 'Komerční banka a.s.' client_name, 
                              '5879879651' client_case_code, 'Stav' status, '18.10.2014' date_f
                       UNION ALL
                       SELECT 1 subject_id, 3 case_id, '885901' case_code, '1.stavební a.s.' client_name, 
                              'S1238' client_case_code, 'Stav' status, '25.11.2014' date_f ) A
                WHERE subject_id = %iN AND case_id = %iN
                ORDER BY 1";
      return $this->db->fetch($query, $subjectId, $caseId);
   }
   
}
