<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 10.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základ DB modelů
 * 
 */

namespace DashboardModule;
 

class FileModel extends \BaseModule\DbModel
{
   const FILE_TYPE_OTHER = 5;

   /**
    * Výpis souborů případu
    * @param int $subjectId Id subjektu
    * @param int $caseId Id případu
    * @return array Pole záznamů
    */
   public function show($subjectId, $caseId)                                                                                               // 
   {
      $query = "SELECT A.subject_id, A.case_id, A.date_f, A.name, A.file
                FROM ( SELECT 1 subject_id, 1 case_id, '02.01.2015 12:55:04' date_f, 'První upomínka' name, 'Prvni_upominka.pdf' file
                       UNION ALL
                       SELECT 1 subject_id, 1 case_id,  '03.02.2015 09:24:52' date_f, 'Dopis od dlužníka' name, 'Dopis.pdf' file
                       UNION ALL
                       SELECT 1 subject_id, 1 case_id,  '20.02.2015 14:28:05' date_f, 'Druhá upomínka' name, 'Druha_upominka.pdf' file ) A
                WHERE subject_id = %iN AND case_id = %iN
                ORDER BY 1 DESC";
      return $this->db->fetchAll($query, $subjectId, $caseId);
   }
   
   public function insertFile($caseId, $fileTypeId, $fileName, $description)
   {
       $query = "INSERT INTO vp_files (vp_file_type_id, vp_file_filename, vp_file_name, vp_file_description)
                VALUES(%iN, %s, %s, %s)";
       $this->db->query($query, $fileTypeId, $fileName, $fileName, $description);
       $fileId = $this->db->insertId();
       
       $query = "INSERT INTO case_files (vp_case_id, vp_file_id)
                VALUES(%i, %i)";
       $this->db->query($query, $caseId, $fileId);
       return $fileId;
   }
   
   /**
    * Seznam dostupných typů souborů pro HTML SELECT
    * @param int $subjectId Id subjektu
    * @return array Pole záznamů
    */
   public function selectFileTypes()
   {
      $query = "SELECT vp_file_type_id as type_id, vp_file_type_description as description
                FROM   vp_file_types ft";
      return $this->db->fetchPairs($query);
   }
}
