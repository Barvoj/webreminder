<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 21.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základ DB modelů
 * 
 */

namespace DashboardModule;
 

class DescriptionModel extends \BaseModule\DbModel
{
   

   /**
    * Výpis souborů případu
    * @param int $subjectId Id subjektu
    * @param int $caseId Id případu
    * @return array Pole záznamů
    */
   public function show($subjectId, $caseId)                                                                                               // 
   {
      $query = "SELECT A.subject_id, A.case_id, A.sentence, A.date_f, A.content
                FROM ( SELECT 1 subject_id, 1 case_id, 1 sentence, '12.01.2015 15:55:10' date_f, 
                              'Zaslána 1. upomínka. Výše dluhu 18 500 Kč.' content
                       UNION ALL
                       SELECT  1 subject_id, 1 case_id, 2 sentence, '02.02.2015 15:55:10' date_f, 
                              'Vystaven splátkový kalendář na 10 měsíčních splátek. Podepsaný a platný.' content ) A
                WHERE subject_id = %iN AND case_id = %iN
                ORDER BY 1 DESC";
      return $this->db->fetchAll($query, $subjectId, $caseId);
   }
   
}
