<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 27.11.2014
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základní třída pro PDF sestavy
 */


namespace BaseModule;


require_once DIR_LIB2 . '/tcpdf/tcpdf.php';
 

class Pdf extends \TCPDF
{
   
   /** @const Mime typ sestavy */
   const REPORT_MIME = 'application/pdf';

   /** @var Presenter Instance presenteru */
   protected $presenter;
  
   /** @var string Označení (kód) sestavy */
   protected $reportCode;

   /** @var string Název sestavy */
   protected $reportName;

   
   /**
    * Vytvoření instance třídy
    * @param string $orientation Orientace sestavy (parametr TCPDF)
    * @param string $format Formát sestavy (parametr TCPDF)
    * @param Presenter $presenter Instance presenteru
    */
   public function __construct ($orientation, $format, $presenter)
   { 
      parent::__construct($orientation, 'mm', $format, TRUE, 'UTF-8', TRUE, TRUE);
      $this->presenter = $presenter;
      $this->SetAuthor($this->presenter->getUser()->getIdentity()->name);
      $this->SetMargins(12, 10, 12);
      $this->SetAutoPageBreak(TRUE, 10);
      $this->setImageScale(PDF_IMAGE_SCALE_RATIO);
   }
    
   /**
    * Standardní patička sestavy
    */
   public function Footer()
   {
      $this->SetY(-10);
   }

}
