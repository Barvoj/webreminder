<?php
/**
 * WebReminder - AKLH
 *
 * Last revison: 9.4.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Třída pro uchování údajů jazyku
 */


namespace BaseModule;

 
class Sms extends \Nette\Object
{

   private $gateInfo = array(
        'gate_url' => 'http://directsmsapi.sluzba.cz',
        'script_on_gate' => "receiver.asp",
        'gate_script_prefix' => "apidreamcom3/",
        'hash_password' => '987654'
   );

    
   public function __construct()
   {
   }

   
  /**
    * Vytvoří ověřovací kód
    * @param  int $phone Telefonní číslo
    * @return string
    */
   public function createCode($phone)
   {
      return strtoupper(substr(uniqid($phone, TRUE), -6));
   }

   
   
   public function send($phone, $text)
   {
      if (empty($phone))
         return false;

      $message = $this->detectConvert(strip_tags($text));
      $hashID = md5($phone . trim(substr($message, 0, 30)) . $this->gateInfo['hash_password']);
      $xmlSMS = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" . PHP_EOL;
      $xmlSMS .= '<batch id="">' . PHP_EOL;
      $xmlSMS .= "<request>textSMS</request>" . PHP_EOL;
      $xmlSMS .= "<hash>" . $hashID . "</hash>" . PHP_EOL;
      $xmlSMS .= '<sender>Collectora</sender>' . PHP_EOL;
      $xmlSMS .= "<recipient>" . $phone . "</recipient>" . PHP_EOL;
      $xmlSMS .= "<content><![CDATA[" . $message . "]]></content>" . PHP_EOL;
      $xmlSMS .= "<udh />" . PHP_EOL;
      $xmlSMS .= '<custid >' . 'AKJV' . '</custid>' . PHP_EOL;
      $xmlSMS .= "<delivery_report>20</delivery_report>" . PHP_EOL;
      $xmlSMS .= "</batch>";
      $url = $this->gateInfo['gate_url'] . "/" . $this->gateInfo['gate_script_prefix'] . $this->gateInfo['script_on_gate'];
      $content = $this->do_post_request($url, $xmlSMS);

      $dom = new \DOMDocument;
      $dom->loadXML($content);
      $statuses = $dom->getElementsByTagName('status');
      foreach ($statuses as $status)
      {
         if ($status->nodeValue !== '200')
            return FALSE;
      }
        return TRUE;
   }

   /**
    * 
    * @param type $url
    * @param type $query_data
    * @return type
    */    
   private function do_post_request($url, $query_data)
   {
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_HEADER, 0);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $query_data);
      $content = curl_exec($ch);
      curl_close($ch);
      return $content;
   }

    
    private function detectConvert($s) {
        $s = str_replace("€", "Eur", $s);

        if (preg_match('#[\x80-\x{1FF}\x{2000}-\x{3FFF}]#u', $s))
            return $this->cs_utf2ascii($s);

        if (preg_match('#[\x7F-\x9F\xBC]#', $s))
            return $this->cs_win2ascii($s);

        return $this->cs_iso2ascii($s);
    }

    // WINDOWS-1250 to ASCII for diacritic chars
    private function cs_win2ascii($s) {
        return strtr($s, "\xe1\xe4\xe8\xef\xe9\xec\xed\xbe\xe5\xf2\xf3\xf6\xf5\xf4\xf8\xe0\x9a\x9d\xfa\xf9\xfc\xfb\xfd\x9e\xc1\xc4\xc8\xcf\xc9\xcc\xcd\xbc\xc5\xd2\xd3\xd6\xd5\xd4\xd8\xc0\x8a\x8d\xda\xd9\xdc\xdb\xdd\x8e", "aacdeeillnoooorrstuuuuyzAACDEEILLNOOOORRSTUUUUYZ");
    }

// ISO-8859-2 to ASCII for diacritic chars
    private function cs_iso2ascii($s) {
        return strtr($s, "\xe1\xe4\xe8\xef\xe9\xec\xed\xb5\xe5\xf2\xf3\xf6\xf5\xf4\xf8\xe0\xb9\xbb\xfa\xf9\xfc\xfb\xfd\xbe\xc1\xc4\xc8\xcf\xc9\xcc\xcd\xa5\xc5\xd2\xd3\xd6\xd5\xd4\xd8\xc0\xa9\xab\xda\xd9\xdc\xdb\xdd\xae", "aacdeeillnoooorrstuuuuyzAACDEEILLNOOOORRSTUUUUYZ");
    }

// UTF-8 to ASCII for diacritic chars
    private function cs_utf2ascii($s) {
        static $tbl = array("\xc3\xa1" => "a", "\xc3\xa4" => "a", "\xc4\x8d" => "c", "\xc4\x8f" => "d", "\xc3\xa9" => "e", "\xc4\x9b" => "e", "\xc3\xad" => "i", "\xc4\xbe" => "l", "\xc4\xba" => "l", "\xc5\x88" => "n", "\xc3\xb3" => "o", "\xc3\xb6" => "o", "\xc5\x91" => "o", "\xc3\xb4" => "o", "\xc5\x99" => "r", "\xc5\x95" => "r", "\xc5\xa1" => "s", "\xc5\xa5" => "t", "\xc3\xba" => "u", "\xc5\xaf" => "u", "\xc3\xbc" => "u", "\xc5\xb1" => "u", "\xc3\xbd" => "y", "\xc5\xbe" => "z", "\xc3\x81" => "A", "\xc3\x84" => "A", "\xc4\x8c" => "C", "\xc4\x8e" => "D", "\xc3\x89" => "E", "\xc4\x9a" => "E", "\xc3\x8d" => "I", "\xc4\xbd" => "L", "\xc4\xb9" => "L", "\xc5\x87" => "N", "\xc3\x93" => "O", "\xc3\x96" => "O", "\xc5\x90" => "O", "\xc3\x94" => "O", "\xc5\x98" => "R", "\xc5\x94" => "R", "\xc5\xa0" => "S", "\xc5\xa4" => "T", "\xc3\x9a" => "U", "\xc5\xae" => "U", "\xc3\x9c" => "U", "\xc5\xb0" => "U", "\xc3\x9d" => "Y", "\xc5\xbd" => "Z");
        return strtr($s, $tbl);
    }
   
}

