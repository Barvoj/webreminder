<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 27.11.2014
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základní třída pro PDF sestavy
 * Formát A4, Portrait
 */


namespace BaseModule;


class PdfA4P extends Pdf
{
   
   /**
    * Vytvoření instance třídy
    * @param Presenter $presenter Instance presenteru
    */
   public function __construct ($presenter)
   { 
      parent::__construct('P', 'A4', $presenter);
   }

   
   public function Header()
   {
      $this->SetY(5);
      $this->SetFont('dejavusans', '', 10);
      $this->Cell(120, 4, 'Společnost', '', 0, 'L', FALSE, '', 1);
      $this->Cell(65, 4, 'List ' . $this->PageNo() . ' / ' . $this->getNumPages(), '', 0, 'R', FALSE, '', 1);
      $this->ln(5);
      $this->SetFont('dejavusans', 'B', 11);
      $this->Cell(185, 6, $this->reportCode . ' - ' . $this->reportName, '', 0, 'L', FALSE, '', 1);
      $this->ln(6);
   }
   
}
