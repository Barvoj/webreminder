<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 10.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Třída pro načítání parametrů aplikace z databáze
 */


namespace BaseModule;
 

class ParamDb
{

   /**
    * Načtení všech parametrů aplikace (defaultní hodnoty před přihlášením)
    * @param \DibiConnection Připojení k DB
    * @return array Pole parametrů ('parametr' => 'hodnota')
    **/ 
   public static function getDefault($db)
   {
      return $db->fetchPairs('SELECT param, value FROM param');
   }

}
