<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 10.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základ DB modelů
 * 
 */

namespace BaseModule;
 

abstract class DbModel extends \Nette\Object
{
   
   /** @var DibiConnection Db připojení */
   protected $db;
   

   public function __construct($db)
   {
      $this->db = $db;
   }


   /**
    * Předání celkového počtu řádků dotazu
    * @return int Celkový počet řádků dotazu
    */
   public function countRows()                                                                                               // 
   {
      return $this->db->fetchSingle('SELECT FOUND_ROWS()');
   }

   
   /**
    * Předání posledního zapsaného id
    * @return int Poslední zapsané id
    */
   public function lastId()
   {
      return $this->db->fetchSingle('SELECT LAST_INSERT_ID()');
   }
   
   
   /**
    * Zahájení transakce
    */
   public function begin()
   {
      return $this->db->begin();
   }

   /**
    * Ukončení transakce
    */
   public function commit()
   {
      return $this->db->commit();
   }
   
   /**
    * Předání aktuálního data a času
    * @return datetime Aktuální datum a čas
    */
   public function sysdate()
   {
      return $this->db->fetchSingle('SELECT now()');
   }

   
   /**
    * Předání aktuálního data(formátovaného)
    * @return string Aktuální datum
    */
   public function sysdateF()
   {
      return $this->db->fetchSingle('SELECT date_output(now())');
   }
   
   
   /**
    * Předání aktuálního času(formátovaného)
    * @return string Aktuální čas
    */
   public function systimeF()
   {
      return $this->db->fetchSingle('SELECT time_output(now())');
   }
   
   
   /**
    * Předání aktuálního data a času(formátovaného)
    * @return string Aktuální datum a čas
    */
   public function sysdatetimeF()
   {
      return $this->db->fetchSingle('SELECT datetime_output(now())');
   }
   
}
