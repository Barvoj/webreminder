<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 19.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Základní DB model pro logování akcí
 */

namespace BaseModule;
 

class LogModel extends DbModel
{
   
   /**
    * Vložení záznamu do log tabulky
    * @param array $data Pole s daty logu
    **/ 
   public function insert($data)
   {
      $query = 'INSERT INTO log (subject_id, case_id, module, presenter, action, status, remote_addr, ip_addr, browser, description, data)
                        VALUES (%iN, %iN, %sN, %sN, %sN, %i, %sN, %sN, %sN, %sN, %sN)'; 
      $this->db->query($query, $data['subject_id'], $data['case_id'], $data['module'], $data['presenter'], $data['action'], $data['status'],
                               $data['remote_addr'], $data['ip_addr'], $data['browser'], $data['description'], $data['data']);
   }

   
   /**
    * Výpis datumu a času posledního přístupu do zadané oblasti
    * @param string $module Název modulu
    * @param string $presenter Název presenteru
    * @param string $action Název akce
    * @return datetime Datum a čas posledního přístupu
    **/ 
   public function lastAccess($module, $presenter, $action)
   {
      $query = 'SELECT date_f FROM log_v
                WHERE subject_id = get_appl_user() AND module = %sN AND presenter = %sN AND action LIKE %sN
                ORDER BY date DESC LIMIT 1';
      return $this->db->fetchSingle($query, $module, $presenter, $action);
   }
   
}
