<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 10.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 */

namespace BaseModule;
 

class SetupModel extends DbModel
{
   
   /**
    * Nastavení aktuálního uživatele pro databázi
    * @param int $id Id uživatele
    **/ 
   public function setUser($id)
   {
      $this->db->query('CALL set_appl_user (%iN)', $id);
   }
   
}
