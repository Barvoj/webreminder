<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 27.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 */


namespace BaseModule;


class LanguagePresenter extends BasePresenter
{

   /*** @var Message Instance modelu zpráv */
   public $logoutDatetime;

   
   protected function startup()
   {
      parent::startup();

   }

   
   /**
    * Akce - Změna jazyka
    * @param string $code Kód jazyka
    * @param string $name Název jazyka
    */
   public function actionChange($backlink, $code, $name)
   {
      $this->language->code = $code;
      $this->language->name = $name;
      $this->restoreRequest($backlink);
      $this->redirect(':Dashboard:Default:default');
   }
   
}
