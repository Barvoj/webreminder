<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 27.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 */


namespace BaseModule;


abstract class LoginPresenter extends BasePresenter
{

   /*** @var Message Instance modelu zpráv */
   public $logoutDatetime;

   
   protected function startup()
   {
      parent::startup();

      if (!$this->getUser()->isLoggedIn())                                                                                        // Je přihlášen uživatel?
      {
         $this->forward(':Sign:Login:step1');                                                                                     // NE - Skok na první krok přihlášení
      }
      else
      {  
         $this->user->setExpiration($this->paramDb['userExpiration'] . ' minutes', TRUE);                                         // Nastavení odhlášení uživatele při nečinnosti      
         $datetime = new \DateTime(date('Y-m-d H:i:s'));                                                                          // Nastavení zobrazení automatického odhlášení
         $datetime->add(new \DateInterval('PT' . $this->paramDb['userExpiration'] . 'M'));
         $this->logoutDatetime = $datetime->format('d.m.Y H:i:s');
         $this->db1->begin();                                                                                                     // Nastavení aktuálního uživatele pro DB
         $setupModel = new SetupModel($this->db1);
         $setupModel->setUser($this->getUser()->getIdentity()->id);
         $this->db1->commit();
      }

   }

   
   /**
    * Akce - Změna jazyka
    * @param string $code Kód jazyka
    * @param string $name Název jazyka
    */
   public function actionLanguage($code, $name)
   {
      $this->language['code'] = $code;
      $this->language['name'] = $name;
      $this->forward(':Dashboard:Default:default');
   }
   
}
