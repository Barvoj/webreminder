<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 27.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 */


namespace BaseModule;


abstract class BasePresenter extends \Nette\Application\UI\Presenter

{
   /** @var string Název vzhledu */
   public $skin = 'skin1';
   
   /** @var array Konfigurační parametry aplikace (pole 'parametr'=>'hodnota' */
   public $paramConfig;

   /** @var array Parametry aplikace z databáze (pole 'parametr'=>'hodnota' */
   public $paramDb;

   /** @var Session */
//   public $common;
   

   /*** @var BaseModule\Log Instance modelu logování */
   protected $logModel;
   
   /** @var DibiConnection Připojení k databázi (vlastní databáze) */
   protected $db1;

   /** @var DibiConnection Připojení k databázi (databáze s daty) */
   protected $db2;

   /** @var HttpResponse HTTP odpověď */
   public $httpResponse;

   /** @var translator Instance translatoru */
   public $translator;   
      
   /** @var translator Instance translatoru */
   public $language;   
      

   protected function startup()
   {
      parent::startup();
      
//      $this->sessionLogin = $this->getSession('login');

      $this->db1 = $this->context->db1;                                                                                           // Otevření databáze 1
      $this->db2 = $this->context->db2;                                                                                           // Otevření databáze 2
      $this->httpResponse = $this->context->getByType('Nette\Http\Response');                                                     // Načtení HTTP odpovědi
      $this->paramConfig = $this->context->getParameters();                                                                       // Načtení konfiguračních parametrů
      $this->paramDb = ParamDb::getDefault($this->db1);                                                                           // Načtení defaultních parametrů z databáze
      
      if (!$this->getSession()->hasSection('language'))
      {
         $this->language = $this->getSession('language');                                                                         // Nastavení defaultního jazyka
         $this->language['code'] = 'cs';
         $this->language['name'] = 'Česky';
      }
      else
      {
         $this->language = $this->getSession('language');                                                                         // Nastavení defaultního jazyka
      }
      $this->translator->setLang($this->language->code);                                                                          // Nastavení jazyka translatoru

      $this->logModel = new LogModel($this->db1);                                                                                 // Zalogovat akci
      $this->writeLog($this->getUser()->isLoggedIn(), NULL);
   }

   

   /**
    * Nastavení názvu vzhledu
    * @return string Název vzhledu
    */
   public function setSkin()
   {
       switch ($this->skin)
      {
         case 'skin1':
               return '/skin1';
         default:
               return '/skin1';
      }
   }

 
   /**
    * Nastavení vyhledávání základních šablon (layoutů)
    * @return array Pole adresářů pro vyhledávání
    */
   public function formatLayoutTemplateFiles()
   {
      $skin = $this->setSkin();
      $name = $this->getName();
      $presenter = substr($name, strrpos(':' . $name, ':'));
      $layout = $this->layout ? $this->layout : 'layout';
      $dir = dirname(dirname($this->getReflection()->getFileName()));
      $list = array("$dir/templates$skin/$presenter/@$layout.latte",
                    "$dir/templates$skin/$presenter.@$layout.latte",);
      do
      {
         $list[] = "$dir/templates$skin/@$layout.latte";
         $dir = dirname($dir);
      } while ($dir && ($name = substr($name, 0, strrpos($name, ':'))));
      return $list;
   }


   /**
    * Nastavení vyhledávání šablon
    * @return array Pole adresářů pro vyhledávání
    */
   public function formatTemplateFiles()
   {
      $skin = $this->setSkin();
      $name = $this->getName();
      $presenter = substr($name, strrpos(':' . $name, ':'));
      $dir = dirname(dirname($this->getReflection()->getFileName()));
      return array("$dir/templates$skin/$presenter/$this->view.latte",
                   "$dir/templates$skin/$presenter.$this->view.latte",);   
   }

   
   public function getModuleName()                                                                                                                                      // Zjištení jména modulu
   {
      $pos = strrpos($this->name, ':');
      if (is_int($pos))
         return str_replace(':', '_', substr($this->name, 0, $pos));
      return NULL;
   }


   public function getPresenterName()                                                                                                                                   // Zjištení jména presenteru (bez modulu)
   {
      $pos = strrpos($this->name, ':');
      if (is_int($pos))
        return substr($this->name, $pos + 1);
      return $this->name;
   }


   protected function getUrl()                                                                                                    // Zjištení aktuálního URL
   {
      $httpRequest = $this->context->getService('httpRequest');
      $url = $httpRequest->getUrl();
      return $url->absoluteUrl;
   }

   
   /**
    * Injektování translatoru
    * @param \GettextTranslator\GetText $translator
    */
   public function injectTranslator(\GettextTranslator\GetText $translator)
   {
      $this->translator = $translator;
   }

   
   /**
    * Vytvoření šablony s vlastním rozšířením
    * @param string $class
    * @return type
    */
   protected function createTemplate($class = NULL)
   {
      $template = parent::createTemplate($class);
      $template->registerHelper('boolean', '\BaseModule\Helpers::boolean');                                                       // Přidání helprů do šablony
      $template->registerHelper('znumber', '\ComModule\Helpers::number');             
      $template->registerHelper('zcurrency', '\ComModule\Helpers::currency');   
      $template->setTranslator($this->translator);
      return $template;
   }

   
   /**
    * Pole logických hodnot
    * @return array
    */
   public function getLogical()
   {
      return array( '1'=>'Ano', '0'=>'Ne');
   }

   
   /**
    * Zápi logu
    * @param int $status Status akce
    * @param string $description Bližší popis akce
    */
   protected function writeLog($status = 0, $description = NULL, $subjectId = NULL, $caseId = NULL)
   {
      if ($this->getUser()->isLoggedIn())
      {
         $userId = $this->getUser()->getIdentity()->subject_id;
         $caseId = $this->getUser()->getIdentity()->case_id;
         $jsonData = json_encode((array) $this->getUser()->getIdentity(),
                     JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
      }
      else
      {
         $userId = $subjectId;
         $caseId = $caseId;
         $jsonData = NULL;
      }
      $data = array('subject_id' => $userId, 
                    'case_id' => $caseId, 
                    'module' => $this->getModuleName(), 
                    'presenter' => $this->getPresenterName(), 
                    'action' => $this->getAction(), 
                    'remote_addr' => gethostbyaddr($_SERVER['REMOTE_ADDR']), 
                    'ip_addr' => $_SERVER['REMOTE_ADDR'], 
                    'browser' => $_SERVER['HTTP_USER_AGENT'], 
                    'status' => $status,
                    'description' => $description . '; ' . $jsonData, 
                    'data' => $this->getUrl());
      $this->db1->begin();
      $this->logModel->insert($data);
      $this->db1->commit();
   }

}
