<?php
/**
 * WebReminder - AKLH
 *
 * Last revison: 21.4.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Třída pro validaci (Utility Class)
 */


namespace BaseModule;

 
class Validator extends \Nette\Object
{

   private function __construct()
   {
   }


   public static function phoneNumber($number)
   {
      $number = str_replace(' ', '', $number);
      if (is_numeric($number))
         return $number;
      return FALSE;
   }
   
   public static function email($email)
   {
      if (filter_var($email, FILTER_VALIDATE_EMAIL))
         return $email;
      return FALSE;
   }

}
