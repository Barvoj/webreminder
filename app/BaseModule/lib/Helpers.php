<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 01.10.2014
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Definice vlastních helperů  (Utility Class)
 */
 
namespace BaseModule;


class Helpers extends \Nette\Object
{


   private function __construct()
   {
   }

   
   public static function boolean($value)
   {
      if ($value > 0)
         return 'Ano';
      else return 'Ne';
   }


   public static function number($value, $decimal = 0)
   {
      $formatter = new \NumberFormatter(\Locale::getDefault(), \NumberFormatter::DECIMAL);
      $formatter->setAttribute(\NumberFormatter::MAX_FRACTION_DIGITS, $decimal);
      return $formatter->format($value);
   }

   
   public static function currency($value, $currency = 'CZK')
   {
      $formatter = new \NumberFormatter(\Locale::getDefault(), \NumberFormatter::CURRENCY);
      return $formatter->formatCurrency($value, $currency);
   }

}
