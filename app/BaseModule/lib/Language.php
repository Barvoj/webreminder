<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 27.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Třída pro uchování údajů jazyku
 */


namespace BaseModule;

 
class Language extends \Nette\Object
{

   public $code;
   public $dbcode;
   public $name;


   public function __construct($code, $dbcode, $name)
   {
      $this->code = $code;
      $this->dbcode = $dbcode;
      $this->name = $name;
   }
   
}

