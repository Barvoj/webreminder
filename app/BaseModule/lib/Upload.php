<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 09.12.2014
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Třída pro upload souborů
 */


namespace BaseModule;


class Upload extends \Nette\Object
{

   private function __construct()
   {
   }
   
     
   static function fileUploaded(\Nette\Forms\Controls\UploadControl $control)
   {
      $file = $control->value;
      return ($file instanceof \Nette\Http\FileUpload && $file->error !== UPLOAD_ERR_NO_FILE);
   }


   static function uploadTest(\Nette\Forms\Controls\UploadControl $control, $maxFileSize)
   {
      $file = $control->value;
      if (!$file instanceof \Nette\Http\FileUpload)
      {
         $control->addError('Přílohu se nepodařilo nahrát.');
         return FALSE;
      }
      elseif ($file->isOk())
      {
         return TRUE;
      }
      else
      {
         switch ($file->error)
         {
            case UPLOAD_ERR_INI_SIZE:
               $control->addError('Velikost přílohy může být nanejvýš ' . TemplateHelpers::bytes($maxFileSize) . '.');
               break;
            case UPLOAD_ERR_NO_FILE:
               $control->addError('Nevybrali jste žádný soubor.');
               break;
            default:
               $control->addError('Přílohu se nepodařilo nahrát.');
               break;
         }
         return FALSE;
      }
   }


   static public function bytes($val)
   {
      $val = trim($val);
      $last = strtolower($val[strlen($val)-1]);
      switch ($last)
      {
         case 'g':
            $val *= 1024;
         case 'm':
            $val *= 1024;
         case 'k':
           $val *= 1024;
      }
      return $val;
   }

}
