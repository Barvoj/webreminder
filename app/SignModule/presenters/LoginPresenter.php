<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 27.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 */


namespace SignModule;

use \Nette\Application\UI\Form;


class LoginPresenter extends \BaseModule\BasePresenter
{  
   public $loginModel;
   public $loginForm;
   public $section;         
   

   protected function startup()
   {
      parent::startup();
      $this->section = $this->getSession('login');         
      $this->loginModel = new LoginModel($this->db1, $this->translator);
      $this->loginForm = new LoginForm($this);
   }


   /**
    * Akce - Přihlášení uživatele do aplikace (krok 1)
    */
   public function actionStep1()
   {
      $this->template->form = $this->loginForm->step1('submitStep1');
   }

   
   /**
    * Submit - Přihlášení uživatele do aplikace
    * @param Nette\Forms\Controls\SubmitButton $button Submit button formuláře
    */
   public function submitStep1(\Nette\Forms\Controls\SubmitButton $button)
   {
      $data = $button->getForm()->getValues();
      try
      {  
         $this->section->data = $this->loginModel->step1($data['subject_code']);                                                  // Přihlášení krok 1 - Otestování kroku
         $this->writeLog(1, $data['subject_code'], $this->section->data['subject_id']);                                           // Zalogování úspěšné akce
         $this->redirect(':Sign:Login:step2');
      }
      catch (\Nette\Security\AuthenticationException $e)                                                                          // Je chyba v kroku 1
      {
         $this->writeLog($e->getCode(), $e->getMessage());                                                                        // Ano - Zalogování akce
         $button->getForm()->addError($e->getMessage());                                                                          // Přidání chyby do formuláře
      }
   }
   

   /**
    * Akce - Přihlášení uživatele do aplikace (krok 2)
    */
   public function actionStep2()
   {
      $this->template->type = $this->typeStep2();
      $this->template->subjectCode = $this->section->data['subject_code'];
      $this->template->form = $this->loginForm->step2($this->template->type, 'submitStep2','submitStep2Back');
   }

   
   /**
    * Submit - Přihlášení uživatele do aplikace - Zpět na krok 1
    * @param Nette\Forms\Controls\SubmitButton $button Submit button formuláře
    */
   public function submitStep2Back(\Nette\Forms\Controls\SubmitButton $button)
   {
      $this->redirect(':Sign:Login:step1');
   }

   
   /**
    * Submit - Přihlášení uživatele do aplikace
    * @param Nette\Forms\Controls\SubmitButton $button Submit button formuláře
    */
   public function submitStep2(\Nette\Forms\Controls\SubmitButton $button)
   {
      $data = $button->getForm()->getValues();
      try
      {
         $loginData = $this->loginModel->step2($this->template->subjectCode, $this->template->type, $data['input']);              // Přihlášení krok 2 - Otestování kroku
         $this->section->data = array_merge($this->section->data, $loginData);                                                    // Přidání údaú do session
         $this->writeLog(1, $this->template->type . '; ' . $data['input'], $this->section->data['subject_id']);                   // Zalogování úspěšné akce
         $this->redirect(':Sign:Login:step3');
      }
      catch (\Nette\Security\AuthenticationException $e)                                                                          // Je chyba autentizace
      {
         $this->writeLog($e->getCode(), $e->getMessage());                                                                        // Ano - Zalogování akce
         $button->getForm()->addError($e->getMessage());                                                                          // Přidání chyby do formuláře
      }
   }

   
   /**
    * Akce - Přihlášení uživatele do aplikace (krok 3)
    */
   public function actionStep3()
   {
      $this->template->form = $this->loginForm->step3($this->section->data['subject_id'], 'submitStep3','submitStep3Back');
   }

   
   /**
    * Submit - Přihlášení uživatele do aplikace - Zpět na krok 2
    * @param Nette\Forms\Controls\SubmitButton $button Submit button formuláře
    */
   public function submitStep3Back(\Nette\Forms\Controls\SubmitButton $button)
   {
      $this->redirect(':Sign:Login:step2');
   }

   
   /**
    * Submit - Přihlášení uživatele do aplikace (krok 3)
    * @param Nette\Forms\Controls\SubmitButton $button Submit button formuláře
    */
   public function submitStep3(\Nette\Forms\Controls\SubmitButton $button)
   {
      $data = $button->getForm()->getValues();
      try
      {
         $phone = $data['phone_new'] == NULL ? $data['phone'] : $data['phone_new'];
         if ($phone == NULL)
         {
            throw new \Nette\Security\AuthenticationException(sprintf($this->translator->translate('Není zadáno nebo vybráno žádné telefonní spojení')),
                                                        LoginModel::ERR_INVALID_STEP3);
         }
         $dataLogin = $this->loginModel->step3($this->section->data['subject_id'], $phone);                                       // Přihlášení krok 3 - Otestování kroku
         $this->section->data = array_merge($this->section->data, $dataLogin);                                                    // Doplnění přihlašovacích dat
         $this->writeLog(1, $phone, $this->section->data['subject_id']);                                                                                                      // Zalogování úspěšné akce
         $this->redirect(':Sign:Login:step4');
      }
      catch (\Nette\Security\AuthenticationException $e)                                                                          // Je chyba autentizace
      {
         $this->writeLog($e->getCode(), $e->getMessage());                                                                        // Ano - Zalogování akce
         $button->getForm()->addError($e->getMessage());                                                                          // Přidání chyby do formuláře
      }
   }
   

   /**
    * Akce - Přihlášení uživatele do aplikace (krok 4)
    */
   public function actionStep4()
   {
      $this->template->form = $this->loginForm->step4($this->section->data['subject_id'], 'submitStep4','submitStep4Back');
   }

   
   /**
    * Submit - Přihlášení uživatele do aplikace - Zpět na krok 3
    * @param Nette\Forms\Controls\SubmitButton $button Submit button formuláře
    */
   public function submitStep4Back(\Nette\Forms\Controls\SubmitButton $button)
   {
      $this->redirect(':Sign:Login:step3');
   }

   
   /**
    * Submit - Přihlášení uživatele do aplikace (krok 4)
    * @param Nette\Forms\Controls\SubmitButton $button Submit button formuláře
    */
   public function submitStep4(\Nette\Forms\Controls\SubmitButton $button)
   {
      $data = $button->getForm()->getValues();
      try
      {
         $dataLogin = $this->loginModel->step4($this->section->data['subject_id'], $data['sms_code']);                            // Přihlášení krok 4 - Otestování kroku
         $this->section->data = array_merge($this->section->data, $dataLogin);                                                    // Doplnění přihlašovacích dat
         $dataActualCase = $this->loginModel->actualCase($this->section->data['subject_id']);                                     // Načtení údajů o aktuálním případu
         $this->section->data = array_merge($this->section->data, $dataActualCase);                                               // Doplnění údajů o aktuálním případu
         $dataCase = $this->loginModel->cases($this->section->data['subject_id']);                                                // Načtení údajů o dostupných případech
         $this->section->data = array_merge($this->section->data, $dataCase);                                                     // Doplnění údajů o dostupných případech
         $this->user->login($this->section->data);                                                                                // Vlastní přihlášení uživatele (subjektu)
         $this->writeLog(1);                                                                                                      // Zalogování úspěšné akce
         unset($this->section->data);                                                                                             // Zrušení dočasné logovací sekce v SESSION
         $this->redirect(':Dashboard:Default:default');
      }
      catch (\Nette\Security\AuthenticationException $e)                                                                          // Je chyba autentizace
      {
         $this->writeLog($e->getCode(), $e->getMessage());                                                                        // Ano - Zalogování akce
         $button->getForm()->addError($e->getMessage());                                                                          // Přidání chyby do formuláře
      }
   }
   

   /**
    * Zjištění typu vstupu u kroku 2
    * @return int 1 = IČ, 2 = Rodné číslo, 3 = Datum narození, -1 = Chyba není ověřovací údaj pro krok 2
    */
   private function typeStep2()
   {
      if (isSet($this->section->data['subject_registration_code']) and $this->section->data['subject_registration_code'] != NULL)
         return 1;
      if (isSet($this->section->data['subject_birth_number']) and $this->section->data['subject_birth_number'] != NULL)
         return 2;
      if (isSet($this->section->data['subject_birth_date']) and $this->section->data['subject_birth_date'] != NULL)
         return 3;
      return -1;
   }
   
   
}
