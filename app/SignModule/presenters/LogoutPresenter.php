<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 19.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 */


namespace SignModule;

use \Nette\Application\UI\Form;


class LogoutPresenter extends \BaseModule\BasePresenter
{
   

   protected function startup()
   {
      parent::startup();
   }


   /**
    * Akce - Odhlášení uživatele z aplikace
    */
   public function actionDefault()
   {
      $this->writeLog(1);                                                                                                         // Zalogování úspěšného odhlášení
      $this->user->logout(TRUE);                                                                                                  // Vlastní odhlášení
      $this->redirect(':Dashboard:Default:default');
   }

}
