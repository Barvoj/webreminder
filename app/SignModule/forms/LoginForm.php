<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 27.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Třída pro vytvoření přihlašovacích formulářů
 */


namespace SignModule;

use \Nette\Application\UI\Form;


class LoginForm extends \Nette\Object
{

   /** @var type Presenter */
   protected $presenter;

   
   /**
    * Vytvoření instance
    * @param type $presenter Presenter
    */
   public function __construct($presenter)
   {
      $this->presenter = $presenter;
   }
    
    
   /**
    * Vytvoření formuláře pro krok 1
    * @param string $action Akce po odeslání formuláře
    * @return Form Instance formuláře
    */
   public function step1($action)
   {
      $form = new Form($this->presenter, 'login1');
      $form->setTranslator($this->presenter->translator);
      $form->addText('subject_code', '', NULL, 255)->addRule(Form::FILLED);
      $form->addSubmit('next', 'Pokračovat')->onClick[] = array($this->presenter, $action);
      return $form;
   }

   
   /**
    * Vytvoření formuláře pro krok 2
    * @param int $type Typ vstupního ůdaje
    * @param string $action Akce po odeslání formuláře
    * @param string $actionBack Akce zpět
    * @return Form Instance formuláře
    */
   public function step2($type, $action, $actionBack)
   {
      $form = new Form($this->presenter, 'login2');
      $form->setTranslator($this->presenter->translator);
      switch ($type)
      {
         case 1:
            $form->addText('input', '', NULL, 10)->addRule(Form::FILLED);
            break;
         case 2:
            $form->addText('input', '', NULL, 11)->addRule(Form::FILLED);
            break;
         case 3:
            $form->addText('input', '', NULL, 10)->addRule(Form::FILLED);
            break;
         default: 
      }
      $form->addSubmit('previous', 'Zpět')->setValidationScope(FALSE)->onClick[] = array($this->presenter, $actionBack);
      if ($type > 0)
         $form->addSubmit('next', 'Pokračovat')->onClick[] = array($this->presenter, $action);
      return $form;
   }

   
   /**
    * Vytvoření formuláře pro krok 3
    * @param int $subjectId Id subjektu
    * @param string $action Akce po odeslání formuláře
    * @param string $actionBack Akce zpět
    * @return Form Instance formuláře
    */
   public function step3($subjectId, $action, $actionBack)
   {
      $form = new Form($this->presenter, 'login3');
      $form->setTranslator($this->presenter->translator);
      $form->addSelect('phone', '', $this->presenter->loginModel->selectPhone($subjectId))->setPrompt(' - Vyberte - ');
      $form->addText('phone_new', '', NULL, 255);
      $form->addSubmit('previous', 'Zpět')->setValidationScope(FALSE)->onClick[] = array($this->presenter, $actionBack);
      $form->addSubmit('next', 'Pokračovat')->onClick[] = array($this->presenter, $action);
      return $form;
   }

   
   /**
    * Vytvoření formuláře pro krok 4
    * @param int $subjectId Id subjektu
    * @param string $action Akce po odeslání formuláře
    * @param string $actionBack Akce zpět
    * @return Form Instance formuláře
    */
   public function step4($subjectId, $action, $actionBack)
   {
      $form = new Form($this->presenter, 'login4');
      $form->setTranslator($this->presenter->translator);
      $form->addText('sms_code', '', NULL, 10)->addRule(Form::FILLED);
      $form->addSubmit('previous', 'Zpět')->setValidationScope(FALSE)->onClick[] = array($this->presenter, $actionBack);
      $form->addSubmit('next', 'Přihlásit')->onClick[] = array($this->presenter, $action);
      return $form;
   }
   
}
