<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 27.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Třída pro autentizaci uživatele
 */


namespace SignModule;


use Nette\Security,
    Nette\Utils\Strings;


class LoginModel extends \Nette\Object
{

   /** Chybové kódy */
   const ERR_INVALID_STEP1 = -1;
   const ERR_INVALID_STEP2 = -2;
   const ERR_INVALID_STEP3 = -3;
   const ERR_INVALID_STEP4 = -4;
   const ERR_INVALID_CASE = -5;
   
   
   /** @var DibiConnection Připojení k DB */
   private $db;

   /** @var Translator Instance translatoru DB */
   private $translator;

   
   public function __construct(\DibiConnection $db, \GettextTranslator\Gettext $translator)
   {
      $this->db = $db;
      $this->translator = $translator;
   }

   
   /**
    * Autentizace uživatele krok 1
    * @param string $subjectCode Identifikační kód subjektu
    * @return array Údaje subjektu
    * @throws Security\AuthenticationException
    */
   public function step1($subjectCode)
   {
      $query = "SELECT 1 step, S.vp_subject_id subject_id, S.vp_subject_code subject_code, S.vp_subject_name subject_name, 
                FROM   vp_subjects S
                WHERE  S.vp_subject_id = %iN";
      $query = "SELECT A.step, A.subject_id, A.subject_code, A.subject_name, 
                       A.subject_birth_number, A.subject_birth_date, A.subject_registration_code
                FROM ( SELECT 1 step, 1 subject_id, '12815' subject_code, 'Novák Josef' subject_name, 
                       '601022/0012' subject_birth_number, '1960-10-22' subject_birth_date, NULL subject_registration_code ) A
                WHERE A.subject_code = %sN";
      $row = $this->db->fetch($query, $subjectCode);
      if (!$row)
      {
      throw new Security\AuthenticationException(sprintf($this->translator->translate('Subjekt s identifikací %s není v evidenci'), $subjectCode), self::ERR_INVALID_STEP1);
      }
      return (array) $row;
   }

   
   /**
    * Autentizace uživatele krok 2
    * @param string $subjectCode Identifikační kód subjektu
    * @param int $typeIdent Typ identifikace ve druhém kroku
    * @param string $stringIdent Obsah identifikace ve druhém kroku
    * @return array Údaje subjektu
    * @throws Security\AuthenticationException
    */
   public function step2($subjectCode, $typeIdent, $stringIdent)
   {
   switch ($typeIdent)
   {
      case 1:
         $query = "SELECT 2 step, S.vp_subject_id subject_id, S.vp_subject_code subject_code, S.vp_subject_name subject_name, 
                          S.vp_subject_birth_number, S.vp_subject_dob birth_date, date_output(S.vp_subject_dob) birth_date_f, 
                          S.vp_subject_registration_number registration_number, S.vp_subject_vat_number vat_number,
             
                   FROM   vp_subjects S
                   WHERE  S.vp_subject_id = %iN";
         $query = "SELECT 2 step, S.vp_subject_id subject_id, S.vp_subject_code subject_code, S.vp_subject_name subject_name, 
                          S.vp_subject_registration_number subject_registration_number
                   FROM   vp_subjects S
                   WHERE  S.vp_subject_code = %sN
                   AND    S.vp_subject_registration_number = %sN";
         $query = "SELECT A.step, A.subject_id, A.subject_code, A.subject_registration_code
                   FROM ( SELECT 2 step, 1 subject_id, '12815' subject_code, NULL subject_registration_code ) A
                   WHERE A.subject_code = %sN AND A.subject_registration_code = %sN";
         $row = $this->db->fetch($query, $subjectCode, $stringIdent);
         if (!$row)
            throw new Security\AuthenticationException(sprintf($this->translator->translate('Subjekt s identifikací %s nemá IČ %s.'), $subjectCode, $stringIdent), self::ERR_INVALID_STEP2);
         break;
      case 2:
         $query = "SELECT 2 step, S.vp_subject_id subject_id, S.vp_subject_code subject_code, S.vp_subject_name subject_name, 
                          S.vp_subject_birth_number birth_number
                   FROM   vp_subjects S
                   WHERE  S.vp_subject_code = %sN
                   AND    S.vp_subject_birth_number = %sN";
         $query = "SELECT A.step, A.subject_id, A.subject_code, A.subject_birth_number
                   FROM ( SELECT 2 step, 1 subject_id, '12815' subject_code, '601022/0012' subject_birth_number ) A
                   WHERE A.subject_code = %sN AND A.subject_birth_number = %sN";
         $row = $this->db->fetch($query, $subjectCode, $stringIdent);
         if (!$row)
            throw new Security\AuthenticationException(sprintf($this->translator->translate('Subjekt s identifikací %s nemá rodné číslo %s.'), $subjectCode, $stringIdent), self::ERR_INVALID_STEP2);
         break;
      case 3:
         $query = "SELECT 2 step, S.vp_subject_id subject_id, S.vp_subject_code subject_code, S.vp_subject_name subject_name, 
                          S.vp_subject_dob birth_date, date_output(S.vp_subject_dob) birth_date_f
                   FROM   vp_subjects S
                   WHERE  S.vp_subject_code = %sN
                   AND    S.vp_subject_dob = %d";
         $query = "SELECT A.step, A.subject_id, A.subject_code, A.subject_birth_date
                   FROM ( SELECT 2 step, 1 subject_id, '12815' subject_code, '1960-10-22' subject_birth_date ) A
                   WHERE A.subject_code = %sN AND A.subject_birth_date = %d";
         $row = $this->db->fetch($query, $subjectCode, $stringIdent);
         if (!$row)
            throw new Security\AuthenticationException(sprintf($this->translator->translate('Subjekt s identifikací %s nemá datum narození %s.'), $subjectCode, $stringIdent), self::ERR_INVALID_STEP2);
         break;
      default:
         throw new Security\AuthenticationException(sprintf($this->translator->translate('Není možná další identifikace subjektu %s v kroku 2'), $subjectCode), self::ERR_INVALID_STEP2);
      }
      return (array) $row;
   }

   /**
    * Autentizace uživatele krok 3
    * @param int $subjectId Id subjektu
    * @param string $phone Číslo telefonu na které bude SMS zaslána
    * @return array Údaje subjektu
    * @throws Security\AuthenticationException
    */
   public function step3($subjectId, $phone)
   {
      $query = 'INSERT INTO sms_code';
      $query = "SELECT A.step, A.subject_id, A.sms_id, A.sms_phone
                FROM ( SELECT 3 step, 1 subject_id, 11 sms_id, 'XXX' sms_phone ) A
                WHERE A.subject_id = %iN";
      $row = $this->db->fetch($query, $subjectId);
      if (!$row)
      {
         throw new Security\AuthenticationException(sprintf($this->translator->translate('Chyba při zasílání SMS kódu')), self::ERR_INVALID_STEP3);
      }
      return (array) $row;
   }

   
   /**
    * Autentizace uživatele krok 4
    * @param int $subjectId Id subjektu
    * @param string $smsCode Přístupový kód zaslaný SMS
    * @return array Údaje subjektu
    * @throws Security\AuthenticationException
    */
   public function step4($subjectId, $smsCode)
   {
      $query = "SELECT A.step, A.subject_id, A.sms_code
                FROM ( SELECT 4 step, 1 subject_id, '111222' sms_code ) A
                WHERE A.subject_id = %iN AND A.sms_code = %sN";
      $row = $this->db->fetch($query, $subjectId, $smsCode);
      if (!$row)
      {
         throw new Security\AuthenticationException(sprintf($this->translator->translate('Přístupový kód %s není platný'), $smsCode), self::ERR_INVALID_STEP4);
      }
      return (array) $row;
   }
   
   
   /**
    * Seznam dostupných případů pro subjekt
    * @param int $subjectId Id subjektu
    * @return array Pole záznamů
    * @throws Security\AuthenticationException
    */
   public function cases($subjectId)
   {
      $query = "SELECT A.subject_id, A.case_id, A.case_code
                FROM ( SELECT 1 subject_id, 1 case_id, '587968' case_code
                       UNION ALL
                       SELECT 1 subject_id, 2 case_id, '712054' case_code
                       UNION ALL
                       SELECT 1 subject_id, 3 case_id, '885901' case_code ) A
                WHERE A.subject_id = %i";
      $row = $this->db->fetchAll($query, $subjectId);
      if (!$row)
      {
         throw new Security\AuthenticationException(sprintf($this->translator->translate('Nejsou přístupné žádné případy')), self::ERR_INVALID_CASE);
      }
      $output = array();
      $output['cases'] = $row;
      return (array) $output;
   }

   
   /**
    * Seznam dostupných případů pro subjekt
    * @param int $subjectId Id subjektu
    * @return array Pole záznamů
    * @throws Security\AuthenticationException
    */
   public function actualCase($subjectId)
   {
      $query = "SELECT A.case_id case_id
                FROM ( SELECT 1 case_id
                       UNION ALL
                       SELECT 2 case_id
                       UNION ALL
                       SELECT 3 case_id ) A
                WHERE 1 = %i LIMIT 1";
      $row = $this->db->fetch($query, $subjectId);
      if (!$row)
      {
         throw new Security\AuthenticationException(sprintf($this->translator->translate('Nejsou přístupné žádné případy')), self::ERR_INVALID_CASE);
      }
   return (array) $row;
   }
   
   
   /**
    * Seznam dostupných telefonních kontaktů pro HTML SELECT
    * @param int $subjectId Id subjektu
    * @return array Pole záznamů
    */
   public function selectPhone($subjectId)
   {
      $query = "SELECT C.vp_contact_col1 phone, C.vp_contact_col1 description
                FROM   vp_contacts C
                JOIN   vp_contact_types T ON T.id = C.vp_contact_type_id
                JOIN   subject_contacts SC ON SC.vp_contact_id = C.vp_contact_id AND SC.subject_id = %iN
                WHERE  C.vp_contact_valid_from <= now()
                AND    C.vp_contact_valid_to >= now()
                AND    T.vp_contact_type_code = 'PHONE'
                ORDER BY C.vp_contact_id DESC";
      $query = "SELECT '+420 758 789 784' phone, '+420 758 789 784' description
                UNION ALL
                SELECT '+420 603 028 512' phone, '+420 603 028 512' description FROM DUAL WHERE 1 = %iN";
      return $this->db->fetchPairs($query, $subjectId);
   }
   
   /**
    * Seznam dostupných emailů pro HTML SELECT
    * @param int $subjectId Id subjektu
    * @return array Pole záznamů
    */
   public function selectEmail($subjectId)
   {
      $query = "SELECT C.vp_contact_col1 email, C.vp_contact_col1 description
                FROM   vp_contacts C
                JOIN   vp_contact_types T ON T.vp_contact_type_id  = C.vp_contact_type_id
                JOIN   subject_contacts SC ON SC.vp_contact_id = C.vp_contact_id AND SC.subject_id = %iN
                WHERE  C.vp_contact_valid_from <= now()
                AND    C.vp_contact_valid_to >= now()
                AND    T.vp_contact_type_code = 'EMAIL'
                ORDER BY C.vp_contact_id DESC";
      $query = "SELECT 'josef.novak@company.com' email, 'josef.novak@company.com' description FROM DUAL WHERE 1 = %iN";
      return $this->db->fetchPairs($query, $subjectId);
   }
   
   /**
    * Seznam dostupných adres pro HTML SELECT
    * @param int $subjectId Id subjektu
    * @return array Pole záznamů
    */
   public function selectAddress($subjectId)
   {
      $query = "SELECT CONCAT_WS('', C.vp_contact_col1, IF(C.vp_contact_col2 IS NULL, '', ' '), C.vp_contact_col2, ', ', C.vp_contact_col4, ' ', C.vp_contact_col6) address,
                    CONCAT_WS('', C.vp_contact_col1, IF(C.vp_contact_col2 IS NULL, '', ' '), C.vp_contact_col2, ', ', C.vp_contact_col4, ' ', C.vp_contact_col6) description
                FROM   vp_contacts C
                JOIN   vp_contact_types T ON T.vp_contact_type_id  = C.vp_contact_type_id
                JOIN   subject_contacts SC ON SC.vp_contact_id = C.vp_contact_id AND SC.vp_subject_id = %iN
                WHERE  C.vp_contact_valid_from <= now()
                AND    C.vp_contact_valid_to >= now()
                AND    T.vp_contact_type_code = 'ADDRESS'
                ORDER BY C.vp_contact_id DESC";
      $query = "SELECT 'U zeleného stromu 12/874, Praha 6 160 00' address, 'U zeleného stromu 12/874, Praha 6 160 00' description FROM DUAL WHERE 1 = %iN";
      return $this->db->fetchPairs($query, $subjectId);
   }
   
   
   /**
    * Vložení nového telefonního kontaktu
    * @param int $subjectId Id subjektu
    * @param string $phone Telefonní kontakt
    * @return array Pole záznamů
    */
   public function insertPhone($subjectId, $phone)
   {
      $query = "INSERT INTO vp_contact (vp_contact_type, vp_contact_valid_from, vp_contact_valid_to,
                                        vp_setting_validity_type_id, vp_contact_col1)
                 ";
      return $this->db->fetchPairs($query, $subjectId);
   }
   
   /**
    * Vložení nového emailového kontaktu
    * @param int $subjectId Id subjektu
    * @param string $email Email
    * @return array Pole záznamů
    */
   public function insertEmail($subjectId, $email)
   {
      $query = "INSERT INTO vp_contact (vp_contact_type, vp_contact_valid_from, vp_contact_valid_to,
                                        vp_setting_validity_type_id, vp_contact_col1)
                 ";
      return $this->db->fetchPairs($query, $subjectId);
   }
   
   
}
