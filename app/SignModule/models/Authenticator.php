<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 11.1.2015
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 * 
 * Třída pro autentizaci uživatele
 */


namespace SignModule;


use Nette\Security,
    Nette\Utils\Strings;


class Authenticator extends \Nette\Object implements Security\IAuthenticator
{

   /** @var DibiConnection Připojení k DB */
   private $db;

   
   public function __construct(\DibiConnection $db)
   {
      $this->db = $db;
   }


   /**
    * Autentizace uživatele
    * @param array $data Pole s daty ze všech přihlašovacích kroků
    * @return Nette\Security\Identity
    */
   public function authenticate(array $data)
   {
       $row = $data[0];
       return new \Nette\Security\Identity($row['subject_id'], NULL, $row);
   }

}
