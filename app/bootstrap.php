<?php
/**
 * WEBREMINDER2
 *
 * Last revison: 10.11.2014
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 *
 * Bootstrap soubor
 */


// Načtení Nette Loaderu
require __DIR__ . '/../vendor/autoload.php';


// Nastavení údajů o verzi aplikace
require __DIR__ . '/version.php';

// Nastavení parematrů - typ vstupního bodu
$params = array();

// Nastavení parematrů - adresáře aplikace
$params['dirApp'] = realpath(__DIR__ . '/../app');
$params['dirCfg'] = realpath(__DIR__ . '/../app/config');													// Konfigurační soubor frameworku
$params['dirLog'] = realpath(__DIR__ . '/../log');														// Logování frameworku
$params['dirTmp'] = realpath(__DIR__ . '/../temp');														// Pracovní adresář frameworku
$params['dirLib'] = realpath(__DIR__ . '/../vendor');														// Knihovna externích scriptů
// $params['dirDocument'] = realpath(__DIR__ . '/../document');													// Soubory s dokumentací k aplikaci

// define("DIR_LIB2", $params['dirLib2']);

/*
define("DIR_APP", $params['dirApp']);
define("DIR_TMP", $params['dirTmp']);
*/

// Vytvoření konfigurátoru
$configurator = new Nette\Configurator;

// Nastavení ladění
$configurator->setDebugMode(TRUE);
$configurator->enableDebugger($params['dirLog']);

// Nastavení adresáře pro cache
$configurator->setTempDirectory($params['dirTmp']);

// Nastavení Loaderu
$configurator->createRobotLoader()
	->addDirectory($params['dirApp'])
	->addDirectory($params['dirLib'])
	->register();
	
// Přidání parametrů
$configurator->addParameters($params);

// Načtení konfigurace
$configurator->addConfig($params['dirCfg'] . '/config.neon', \Nette\Configurator::AUTO);
// $configurator->addConfig($params['dirCfg'] . '/config.prod.neon');

// Vytvoření kontejneru
$container = $configurator->createContainer();
//$container->router = new Nette\Application\Routers\SimpleRouter('User:Home:default');

// Nastavení routování
$container->router = new Nette\Application\Routers\SimpleRouter('Dashboard:Default:default', Nette\Application\Routers\SimpleRouter::SECURED);

return $container;
