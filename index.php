<?php
/**
 * K-PROJECT
 *
 * Last revison: 26.11.2014
 * @copyright	Copyright (c) 2014 collectora software s.r.o. <http://www.collectora.cz>
 */


// Odkomentovat při údržbě aplikace
// require '.maintenance.php';


// Vytvoření konteineru
$container = require __DIR__ . '/app/bootstrap.php';

$container->getService('application')->run();
