function disableStep() {
    $('input[type="submit"]').prop('disabled', true);
}
function enableStep() {
    $('input[type="submit"]').prop('disabled', false);
}

/**
 * Doplněk ke knihovně Resumable.js.
 * 
 * Naváže se na $element (input file) a po kliknutí na něj a vybrání souboru
 * je tento $element nahrazen divem obsahujícím progress bar (jQuery UI) a soubor
 * se začne ihned na pozadí přenášet knihovnou Resumable.js.
 * 
 * Po dokončení nahrávání souboru již ve formuláři nebude původní $element (input file)
 * ale místo něj tam budou dva input texty - se jménem souboru a s identifikátorem nahraného
 * souboru na serveru (typicky jméno dočasného souboru)
 * 
 * @param {type} $element Input file
 * @param {type} query
 * @returns {Uploadable}
 */
function Uploadable( $element, query )
{
    if(query === undefined) {
        query = {};
    } else {
        // klonování objektu
        query = JSON.parse(JSON.stringify(query));
    }

    this.fileId = undefined;
    this.query = query;
    
    this.$input = $element;
    
    this.$container = $( '<div>' ).attr({'class':'uploadable-container form-control', style: "padding: 3px 12px;"});
    this.$label = $('<div>').attr({style: "float: left; width: 48%; height: 30px; line-height: 30px; overflow: hidden; margin-right: 2%;"});
    this.$btnDelete = $('<button>').attr(this.btnAttr).text(this.localization[this.locale].remove);
    this.$hidenFileName = $('<input>').attr({type: 'hidden', name: this.$input.attr('name')+'-name'});
    this.$hidenFileId = $('<input>').attr({type: 'hidden', name: this.$input.attr('name')+'-id'});
    
    this.$progressBarLabel = $('<div>').attr({'class': 'progress-label'});
    this.$progressBar = $('<div>').attr({'class': 'progressbar'}).append(this.$progressBarLabel);
    
    var $div2 = $('<div>').attr({style: "float: left; width: 30%;"}).html(this.$progressBar);
    var $div3 = $('<div>').attr({style: "float: left; width: 20%; text-align: right;"}).html(this.$btnDelete);

    this.$container.html(this.$label);
    this.$container.append($div2);
    this.$container.append($div3);
    this.$container.append($('<div>').attr({style: "clear: both;"}));
    this.$container.append(this.$hidenFileName);
    
    this.initResumable();
}

/**
 * Atributy tlačítka
 * @type Object
 */
Uploadable.prototype.btnAttr = {'class': 'btn btn-default btn-sm'};

/**
 * @type Object
 */
Uploadable.prototype.localization = {};
Uploadable.prototype.localization['cs'] = {
    remove: 'Smazat',
    done: 'Hotovo..'
};

Uploadable.prototype.locale = 'cs';

/**
 * Inicializuje knihovnu resumable.js
 * @returns {undefined}
 */
Uploadable.prototype.initResumable = function()
{
    var obj = this;
    var r = new Resumable({
        target: '?action=statement&presenter=Dashboard%3AFile&do=upload', 
        simultaneousUploads: 1,
        maxFiles: 1,
        query: this.query
    });
    this.resumable = r;
    r.assignBrowse(this.$input);

    r.on('fileAdded', function(file){
        obj.addFile(file);
        r.upload();
    });
    r.on('fileSuccess', function(file, response){
        var json = JSON.parse(response);
        obj.setFileId(json.fileName);
    });
    r.on('uploadStart', function(){
        disableStep();
    });
    r.on('complete', function(){
        enableStep();
    });
    r.on('progress', function(val){
        var perc = Math.round(r.progress()*100);
        obj.updateProgress(perc);
    });
    r.on('error', function(message, file){
        enableStep();
    });
};

/**
 * Inicializuje jQuery UI progress bar
 * @returns {undefined}
 */
Uploadable.prototype.initProgressBar = function ()
{
    var _this = this;

    _this.$progressBarLabel.text('0 / 100');
    _this.$progressBar.progressbar({
        value: false,
        max: 100,
        change: function () {
            _this.$progressBarLabel.text(_this.$progressBar.progressbar("value") + " / " + _this.$progressBar.progressbar("option", "max"));
        },
        complete: function () {
            _this.$progressBarLabel.text(_this.localization[_this.locale].done);
        }
    });
};

/**
 * Smaze soubor a zobrazi input pro vlozeni noveho
 * @param {ResumableFile} file
 */
Uploadable.prototype.removeFile = function ( file )
{
    this.unsetFileId();
    this.resumable.removeFile(file);
    this.$container.replaceWith(this.$input);
};

/**
 * Odstrani input pro vlozeni souboru, zobrazi nazev vlozeneho souboru a progress bar
 * @param {ResumableFile} file
 */
Uploadable.prototype.addFile = function ( file )
{
    var callbackRemoveFile = (function(obj) { return function(e) { e.stopPropagation(); e.preventDefault(); obj.removeFile(file); }; })(this);
    
    this.$btnDelete.on('click', callbackRemoveFile);
    this.$label.attr({title: file.fileName}).html(file.fileName);
    this.$hidenFileName.val(file.fileName);
    this.initProgressBar();
    
    this.$input.replaceWith(this.$container);
};

Uploadable.prototype.setFileId = function ( fileId )
{
    this.fileId = fileId;
    this.$hidenFileId.val(fileId);
    this.$container.append(this.$hidenFileId);
};

Uploadable.prototype.unsetFileId = function ()
{
    this.fileId = undefined;
    this.$hidenFileId.val('');
    this.$hidenFileId.remove();
};

Uploadable.prototype.updateProgress = function(perc)
{
    this.$progressBar.progressbar("value", perc);
};