/**
 * Umožňuje provádět kontrolu, zda datum není velikonoční pondělí, svátek nebo víkend.
 * @returns { HolidayChecker }
 */
function HolidayChecker() {}

/**
 * Data velikonoc. { rok: [den, mesic] }
 * @type Object
 */
HolidayChecker.prototype.easterDays = {
    2015: [6,4],
    2016: [28,3],
    2017: [17,4],
    2018: [2,4],
    2019: [22,4],
    2020: [13,4],
    2021: [5,4],
    2022: [18,4],
    2023: [10,4],
    2024: [1,4],
    2025: [21,4],
    2026: [6,4],
    2027: [29,3],
    2028: [17,4],
    2029: [2,4],
    2030: [22,4]
};

/**
 * Kontrola, zda datum není velikonoční pondělí.
 * @param { Date } date
 * @returns { Boolean }
 */
HolidayChecker.prototype.isEaster = function( date ) {
    var y = date.getFullYear();
    if( this.easterDays[y] !== undefined )
    {
      if (date.getMonth() == this.easterDays[y][1]-1
            && date.getDate() == this.easterDays[y][0]) {
          return true;
        }
    }
    return false;
};

/**
 * Data všech svátků. [Den, Mesíc, Název svátku]
 * @type Array
 */
HolidayChecker.prototype.holidays = [
    [1, 1, 'Den obnovy samostatného českého státu'], 
    [1, 5, 'Svátek práce'], 
    [8, 5, 'Den vítězství'],
    [5, 7, 'Den slovanských věrozvěstů Cyrila a Metoděje'], 
    [6, 7, 'Den upálení mistra Jana Husa'], 
    [28, 9, 'Den české státnosti'],
    [28, 10, 'Den vzniku samostatného československého státu'], 
    [17, 11, 'Den boje za svobodu a demokracii'], 
    [24, 12, 'Štědrý den'],
    [25, 12, '1. svátek vánoční'], 
    [26, 22, '2. svátek vánoční']
];

/**
 * Kontrola, zda datum není svátek (včetně velikonoc)
 * @param { type } date
 * @returns { Boolean }
 */
HolidayChecker.prototype.isHoliday = function(date) {
    for (i = 0; i < this.holidays.length; i++) {
        if (date.getMonth() == this.holidays[i][1]-1
                && date.getDate() == this.holidays[i][0]) {
              return true;
        }
    }
    return this.isEaster(date);
};

/**
 * Kontrola, zda datum není Sobota nebo Neděle.
 * @param { type } date
 * @returns { Boolean }
 */
HolidayChecker.prototype.isWeekend = function(date) {
    return !$.datepicker.noWeekends(date)[0];
};

/**
 * Kontrola, zda je datum pracovní den.
 * @param { type } date
 * @returns { Boolean }
 */
HolidayChecker.prototype.isWorkDay = function( date ) {
    if( this.isWeekend( date ) ) {
        return false;
    } else if( this.isHoliday( date ) ) {
        return false;
    }
    return true;
};