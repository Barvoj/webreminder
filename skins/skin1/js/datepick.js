/**
 * Pomocná třída pro nastavení jQuery UI datepickeru.
 * 
 * V datepickeru budou zakázány víkendy a české svátky.
 * 
 * Typycké použití: kdy se má zákazníkovi ozvat firma. 
 * 
 * @param {type} $element
 * @returns {DatePick}
 */
function DatePick($element)
{
    function noWeekendsOrHolidays(date) {
        var holidayChecker = new HolidayChecker();

        if( holidayChecker.isWorkDay( date ) ) {
            return [true, ''];
        } else {
            return [false, ''];
        }
    }

    var minDate;
    var currentHour = new Date().getHours();
    // Pokud je po criticalHour, již nelze hovory domlouvat na beforeCriticalHour ale až od afterCriticalHour
    if (currentHour < this.criticalHour) {
       minDate = this.beforeCriticalHour;
    } else {
       minDate = this.afterCriticalHour;
    }

    $element.datepicker({
        firstDay: 1,
        minDate: minDate,
        //  maxDate: "+1m",
        dateFormat: this.dateFormat,
        beforeShowDay: noWeekendsOrHolidays
    });
}

DatePick.prototype.dateFormat = 'dd.mm.yy';

/**
 * Kritická hodina v rámci dne. Před touto hodinou je minimální datum, které lze 
 * v datepickeru zvolit, beforeCriticalHour a po této hodině je to až afterCriticalHour
 * 
 * Typicky do 18 hod lze zvolit zítřek, po 18 hod již lze zvolit nejdříve pozítřek.
 * @type Number
 */
DatePick.prototype.criticalHour = 18;
DatePick.prototype.beforeCriticalHour = 1;
DatePick.prototype.afterCriticalHour = 2;