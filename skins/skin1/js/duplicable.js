/**
 * Slouží k duplikování inputů ve formuláři.
 * 
 * Vstupem je kontejner na tyto inputy. V tomto kontejneru musí být jeden prázdný
 * řádek (=s nevyplněnými inputy).
 * 
 * Do kontejneru se automaticky přidá tlačítko pro přidáná dalšího řádku. Po kliknutí
 * dojdek naklonování prázdného řádku a tento klon se pak přidá do kontejneru.
 * 
 * @param {type} $container Kontejner na řádky.
 * @param {type} options 
 * @returns {Duplicable}
 */
function Duplicable( $container, options )
{
    if(options === undefined) options = {};
    this.init(options);
    /**
     * Kontejner na řádky
     */
    this.$container = $container;
    /**
     * Prázdný řádek, který se používá pro duplikování
     */
    this.$emptyRow = this.$container.find('.duplicable-row.duplicable-empty');
    // Kontrola existence právě jednoho prázdného řádku
    if(this.$emptyRow.length !== 1) {
        console.error('1 empty row required, but there is '+this.$emptyRow.length+' row(s)');
    }
    this.$emptyRow.remove(); // Odstranění prázdného řádku, aby se nevyskytoval v DOM
    
    /**
     * Pořadové číslo řádku, který bude vytvořen jako následující (voláním metody addNext)
     */
    this.nextNumber = this.$container.find('.duplicable-row:not(.duplicable-empty)').length + 1; // počet NEprázdných řádků + 1
    
    // callback po kliknutí na tlačítko Add
    var callback = (function(_this) { return function(e) {
            e.stopPropagation();
            e.preventDefault();
            _this.addNext();
    }; })(this);
    
    this.$maxRowNumber = $('<input></input>').attr({type: 'hidden', name: 'duplicable-max-row-number', value: this.nextNumber-1});
    this.$container.append(this.$maxRowNumber);
    
    /**
     * Tlačítko pro přidání nového řádku
     */
    this.$btnAdd = $('<button></button>').attr(this.btnAttr).text(this.localization[this.locale].addNext).on('click', callback);
    this.$container.append(this.$btnAdd);
    
    // pokud není vložen žádný řádek, přidá se aspoň jeden.
    if(this.nextNumber === 1) {
        this.addNext();
    }
}

/**
 * Atributy tlačítka
 * @type Object
 */
Duplicable.prototype.btnAttr = {'class': 'btn btn-primary'};

/**
 * 
 * @type Object
 */
Duplicable.prototype.localization = {};
Duplicable.prototype.localization['cs'] = {
    addNext: 'Přidat další'
};

Duplicable.prototype.locale = 'cs';

Duplicable.prototype.init = function( options )
{
    if(options.beforeAppendRow !== undefined)
    {
        this.beforeAppendRow = options.beforeAppendRow;
    }
    if(options.afterAppendRow !== undefined)
    {
        this.afterAppendRow = options.afterAppendRow;
    }
};

/**
 * Pokud již existují v DOM neprázdné řádky, tato metoda notifikuje  o jejich počtu.
 * @param {int} count Počet již existujících řádků na stránce
 * @returns {Duplicable.prototype}
 */
Duplicable.prototype.setRowCount = function( count )
{
    this.nextNumber = ++count;
    return this;
};

/**
 * Vrací kopii prázdného řádku, kterou je možné vložit do DOM
 * @returns {Duplicable.prototype@pro;$emptyRow@call;clone}
 */
Duplicable.prototype.getEmptyRow = function()
{
    return this.$emptyRow.clone();
};

/**
 * Vloží nový řádek.
 */
Duplicable.prototype.addNext = function()
{
    var $row = this.getEmptyRow();
    this.$maxRowNumber.val(this.nextNumber);
    var nextNumber = this.nextNumber++;

    this.beforeAppendRow( $row, nextNumber );
    $row.insertBefore(this.$btnAdd);
    this.afterAppendRow( $row, nextNumber );

    $row.removeClass('duplicable-empty');
};

/**
 * Tato metoda se volá PŘED vložením nového řádku do DOM.
 * @param {type} $row
 * @param {int} rowNumber
 */
Duplicable.prototype.beforeAppendRow = function( $row, rowNumber ) {};

/**
 * Tato metoda se volá PO vložením nového řádku do DOM.
 * @param {type} $row
 * @param {int} rowNumber
 */
Duplicable.prototype.afterAppendRow = function( $row, rowNumber ) {};