function getAres(link, field)
{
   var rn = $("input[name='" + field + "']").val();
   if (rn == '')
      alert('Musí být vyplněno pole IČ');
   else var jqxhr = $.post(link, { rn: rn }, function(data) { getAresData(data); } );
}


function getAresData(data)
{
   var ret = $.parseJSON(data);
   if (ret.stav == 'ok')
   {
      $("input[name='name']").val(ret.firma);
      $("input[name='vat_code']").val(ret.dic);
      $("input[name='addr_street']").val(ret.ulice);
      $("input[name='addr_village']").val(ret.mesto);
      $("input[name='addr_zip_code']").val(ret.psc);
      if (ret.dic != '')
      {
         $("select[name='is_vat']").val(1);
      }
      else
      {
         $("select[name='is_vat']").val(0);
      }
   }
   else
   {
      alert(ret.stav);
   }
}
