$(document).ready(function($){    
var link = {plink :Project:Management:itemwhisperercode};
link = link.replace('\\', '');
var link2 = {plink :Project:Management:itemgetprice};
link2 = link2.replace('\\', '');
var pricelist = {$proposal->pricelist_id};
$(".autocomplete_code").autocomplete
   ({
      source: link,
      minLength: 2,
      search: function( event, ui )
      { 
         $(this).autocomplete('option', 'source', link + '&pricelist=' + pricelist + '&vendor=' + $('.autocomplete_vendor').val() );
      },     
      select: function(event, ui)
      {
         $(this).val(ui.item.label);
  	 $(".autocomplete_name").val(ui.item.name);
  	 $(".autocomplete_price").val(ui.item.price_f);
      },
      change: function()
      {
         var vendor = $('.autocomplete_vendor').val();
         var code = $('.autocomplete_code').val();
         $.ajax(
         {
            url: link2,
            cache: false,
            type: "GET",
            data: { 'pricelist': pricelist, 'vendor': vendor, 'code': code },
            success: function(data)
            {
              var ret = $.parseJSON(data);
              $(".autocomplete_price").val(ret.price_f);
              $(".autocomplete_name").val(ret.item_name);
            }
         })
      }
   });    
});
