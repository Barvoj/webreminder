function itemwhisperercode(link, link2, pricelist)
{
$(".autocomplete_code").autocomplete
   ({
      source: link,
      minLength: 2,
      search: function( event, ui )
      { 
         $(this).autocomplete('option', 'source', link + '&pricelist=' + pricelist + '&vendor=' + $('.autocomplete_vendor').val() );
      },     
      select: function(event, ui)
      {
         $(this).val(ui.item.label);
  	 $(".autocomplete_name").val(ui.item.name);
  	 $(".autocomplete_price").val(ui.item.price_f2);
  	 $(".autocomplete_uon").text(ui.item.uom);
      },
      change: function()
      {
         var vendor = $('.autocomplete_vendor').val();
         var code = $('.autocomplete_code').val();
         $.ajax(
         {
            url: link2,
            cache: false,
            type: "GET",
            data: { 'pricelist': pricelist, 'vendor': vendor, 'code': code },
            success: function(data)
            {
              var ret = $.parseJSON(data);
              $(".autocomplete_price").val(ret.price_f2);
              $(".autocomplete_name").val(ret.item_name);
            }
         })
      }
   });    
}


function itemwhisperername(link, link2, pricelist)
{
$(".autocomplete_name").autocomplete
   ({
      source: link,
      minLength: 2,
      search: function( event, ui )
      { 
         $(this).autocomplete('option', 'source', link + '&pricelist=' + pricelist + '&vendor=' + $('.autocomplete_vendor').val() );
      },     
      select: function(event, ui)
      {
         $(this).val(ui.item.label);
  	 $(".autocomplete_code").val(ui.item.code);
  	 $(".autocomplete_price").val(ui.item.price_f);
      },
      change: function()
      {
         var vendor = $('.autocomplete_vendor').val();
         var code = $('.autocomplete_code').val();
         $.ajax(
         {
            url: link2,
            cache: false,
            type: "GET",
            data: { 'pricelist': pricelist, 'vendor': vendor, 'code': code },
            success: function(data)
            {
              var ret = $.parseJSON(data);
              $(".autocomplete_price").val(ret.price_f);
            }
         })
      }
   });    
}

