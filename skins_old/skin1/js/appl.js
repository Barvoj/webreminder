function show (selector)
{
   $(selector).show();
} 


function submenuVisible (id, tag)
{
   if ($('#' + id).is(':visible'))
   {
      $('#' + id).hide(400);
      $('#' + tag).toggleClass('fa-folder-open-o', false);
      $('#' + tag).toggleClass('fa-folder-o', true);
      if(typeof(Storage) !== "undefined")
      {
         localStorage.setItem('submenu-' + id, 'F');
      }
   } 
   else
   {
      $('#' + id).show(400);
      $('#' + tag).toggleClass('fa-folder-o', false);
      $('#' + tag).toggleClass('fa-folder-open-o', true);
      if(typeof(Storage) !== "undefined")
      {
         localStorage.setItem('submenu-' + id, 'T');
      }
   } 
} 


function submenuVisibleSetup (cssClass)
{
   jQuery.each($('.' + cssClass), function()
   {
      if(typeof(Storage) !== "undefined")
      {
         id = $(this).next('ul:first').attr('id');
         idIcon = $(this).find('i:last').attr('id');
         if (typeof localStorage.getItem('submenu-' + id) !== 'undefined')
         {
            if (localStorage.getItem('submenu-' + id) == 'T')
            {
               $('#' + id).show();
               $('#' + idIcon).toggleClass('fa-folder-o', false);
               $('#' + idIcon).toggleClass('fa-folder-open-o', true);
            }
            else
            {
               $('#' + id).hide();
               $('#' + idIcon).toggleClass('fa-folder-open-o', false);
               $('#' + idIcon).toggleClass('fa-folder-o', true);
            }
         }
      }
   }); 
} 


function filterParamVisible (cssClass, storage)
{
   if ($('.' + cssClass).is(':visible'))
   {
      $('.' + cssClass).hide(400);
      if(typeof(Storage) !== "undefined")
      {
         localStorage.setItem(cssClass + storage, 'F');
      }
   } 
   else
   {
      $('.' + cssClass).show(400);
      if(typeof(Storage) !== "undefined")
      {
         localStorage.setItem(cssClass + storage, 'T');
      }
   } 
} 


function filterParamVisibleSetup (cssClass, storage)
{
   if(typeof(Storage) !== "undefined")
   {
      if (typeof localStorage.getItem(cssClass + storage) !== 'undefined')
      {
         if (localStorage.getItem(cssClass + storage) == 'T')
         {
            $('.' + cssClass).show();
         }
         else
         {
            $('.' + cssClass).hide();
         }
      }
      else
      {
         $('.' + cssClass).hide();
      }
   } 
} 


function saveScroll() // added function
{
   if (typeof(Storage) !== "undefined")
   {
      localStorage.setItem('X-Position', $(window).scrollLeft());
      localStorage.setItem('Y-Position', $(window).scrollTop());
   }
}


function loadScroll()
{
   if (typeof(Storage) !== "undefined")
   {
/*      alert('y-' + localStorage.getItem('Y-Position'));*/
/*      newPos = new Object();
      newPos.left = localStorage.getItem('X-Position');
      newPos.top = 50;
      $(window).offset(newPos);*/           
/*      window.scrollTo(localStorage.getItem('X-Position'), localStorage.getItem('Y-Position'));*/
      $(window).scrollLeft(localStorage.getItem('X-Position'));
      $(window).scrollTop(localStorage.getItem('Y-Position'));
   }
}
